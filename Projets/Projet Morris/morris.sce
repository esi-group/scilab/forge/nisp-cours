function y = morris(x,b0,b1,b2,b3,b4)
    //
    // The function definition is 
    //
    // <latex>
    // f(x)=\beta_0 
    // + \sum_{i=1}^{20} \beta_i w_i
    // + \sum_{i<j}^{20} \beta_{i,j} w_i w_j
    // + \sum_{i<j<k}^{20} \beta_{i,j,k} w_i w_j w_k
    // + \sum_{i<j<k<l}^{20} \beta_{i,j,k,l} w_i w_j w_k w_l
    // </latex>
    //
    // where
    //
    // <latex>
    // w_i=2(x_i-1/2), \quad i=1,...,20
    // </latex>
    //
    // except for i=3,5,7, for which :
    //
    // <latex>
    // w_i=2\left(\frac{1.1 x_i}{x_i+0.1}-1/2\right), \quad i=3,5,7,
    // </latex>
    //
    // for any x(i) in [0,1], and i=1,...,20.
    //
    // Reference
    // Factorial Sampling Plans for Preliminary Computational Experiments
    // Max D. Morris, Technometrics, Vol. 33, No. 2. (May, 1991), pp. 161-174
    //
    // A. Saltelli, K. Chan and E. M. Scott eds, 2000, 
    // Sensitivity Analysis, Wiley, Section 2.9
    // 
    // Package ‘sensitivity’
    // Gilles Pujol, Bertrand Iooss, Alexandre Janon
    
    // Compute the weights w
    w = 2 * (x - 0.5)
    i357=[3,5,7]
    w(:,i357) = 2 * (1.1 * x(:,i357) ./ (x(:,i357) + .1) - 0.5)
    // Compute the output y
    y = b0
    for i= 1 : 20
        y = y + b1(i) * w(:,i)
    end
    for i= 1 : 19
        for j = (i + 1) : 20
            y = y + b2(i,j) * w(:,i) .* w(:,j)
        end
    end
    for i= 1 : 18
        for j = (i + 1) : 19
            for k = (j + 1) : 20
                y = y + b3(i,j,k) * w(:,i) .* w(:,j) .* w(:,k)
            end
        end
    end
    for i= 1 : 17
        for j = (i + 1) : 18
            for k = (j + 1) : 19
                for l = (k+1):20
                    y = y + b4(i,j,k,l) * w(:,i) .* w(:,j) .* w(:,k) .* w(:,l)
                end
            end
        end
    end
endfunction

b0 = distfun_normrnd(0,1);
x=distfun_normrnd(0,1,1,10);
b1 = [repmat(20,1,10),x];
b2 = distfun_normrnd(0,1,20,20);
b2(1:6,1:6)=-15;
b3 = zeros(20,20,20);
b3(1:5,1:5,1:5)=-10;
b4=zeros(20,20,20,20);
b4(1:4,1:4,1:4,1:4)=5;

nx=20
n=100
x=distfun_unifrnd(0,1,n,nx);
y = morris(x,b0,b1,b2,b3,b4);

function x=myrandgen(m, i)
    x = distfun_unifrnd(0,1,m,1)
endfunction

n = 1000;
nx = 20;
[s,nbevalf,smin,smax]=nisp_sobolsaFirst(list(morris,b0,b1,b2,b3,b4),nx,myrandgen,n)
[st,nbevalf,stmin,stmax]=nisp_sobolsaTotal(list(morris,b0,b1,b2,b3,b4),nx,myrandgen,n)
nisp_plotsa([s,smin,smax],[st,stmin,stmax]);
mprintf("First order sensitivity indices:\n")
mprintf("S, Lower Bound, Upper Bound\n")
disp([s,smin,smax])
mprintf("Total sensitivity indices:\n")
mprintf("ST, Lower Bound, Upper Bound\n")
disp([st,stmin,stmax])


