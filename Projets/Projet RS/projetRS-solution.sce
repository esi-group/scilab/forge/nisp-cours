// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Modelisation et Simulation (M2S)
// Module Informatique scientifique approfondie (I1)
// Traitement des incertitudes (I1C)
// jean-marc.martinez@cea.fr
// michael.baudin@edf.fr
// Examen : 2012 - 2013
// Projet R-S
// Solution

// Reference
// F. Deheeger
// "Couplage mecano-fiabiliste : 2SMART, 
// methodologie d'apprentissage stochastique en fiabilite."
// These, 2008
// "IV.4.1 Validation de l'apprentissage
// la methode SMART, Le cas R-S"
// p 118

// Analytical model definition:
function G = LimitState ( x )
    R = x(:,1)
    S = x(:,2)
    G = R-S
endfunction

// 1. Test of the limit state function:
x = [
7 2
-6 1
8 3
];
strx=string(x(:,1))+","+string(x(:,2))
mprintf ("G(%s)=%f\n" , strx, LimitState(x) );

// Variable R - Normale(7,1)
muR=7;
sigmaR=1;
// Variable S - Normale(2,1)
muS=2;
sigmaS=1;

//
// 2. Exact computation
// when R and S are normal
mu=muR-muS;
sigma=sqrt(sigmaR^2+sigmaS^2);
pfexact=distfun_normcdf(0,mu,sigma)

// 3. Generate a Simple Random Sampling
NbSim=100000;
// Variable R
R=distfun_normrnd(muR,sigmaR,NbSim,1);
// Variable S
S=distfun_normrnd(muS,sigmaS,NbSim,1);

// 4. Histograms of R and S
scf();
subplot(1,2,1)
histplot(20,R)
xtitle("PDF of R","R","Frequency")
subplot(1,2,2)
histplot(20,S)
xtitle("PDF of S","S","Frequency")

// 5. Put the two histograms on the same plot
h=scf();
histplot(20,R,style=2)
histplot(20,S,style=5)
xtitle("Test R-S","","Frequency")
legend(["R","S"])
h.children.children(3).children.line_style=2;

// 6. Monte-Carlo
x=[R,S];
G = LimitState ( x );

// 7. Histogram of R-S
scf();
histplot(20,G)
xtitle("Test R-S","R-S","Frequency")
mprintf("Number of simulations=%d\n" , NbSim);

// 8. Estimate Pf
failed = find(G<0);
nfail = size(failed,"*")
Pf = nfail/NbSim;
mprintf("Number of failures=%d\n" , nfail);
mprintf("Pf = %e\n" , Pf);

// 9. Compute a confidence interval of Pf
level=1.-0.95;
q = level/2.
p = 1.-q
f = distfun_norminv(p,0.,1.)
low = Pf - f * sqrt(Pf*(1.-Pf)/NbSim)
up = Pf + f * sqrt(Pf*(1.-Pf)/NbSim)
mprintf("95%% Conf. Int.:[%e,%e]\n" , low,up);

// 10. Create a figure of failures
// See "Fig. IV.2 [Cas R - S]", [1], p124
scf();
succ = find(G>0);
plot(R(succ),S(succ),"bo");
failed= find(G<0);
plot(R(failed),S(failed),"rx");
xtitle("Test R-S","R","S");
legend(["Success","Failures"]);

// 11. Convergence des estimateurs
imax=23;
stacksize("max");
nbsimarray=[];
relerr=[];
mprintf("NbSim, nfail, Pf:\n" );
for i=1:imax
    NbSim=2^i;
    R=distfun_normrnd(muR,sigmaR,NbSim,1);
    S=distfun_normrnd(muS,sigmaS,NbSim,1);
    x=[R,S];
    G = LimitState ( x );
    failed = find(G<0);
    nfail = size(failed,"*");
    Pf = nfail/NbSim;
    mprintf("%7d, %5d, %e\n" , NbSim, nfail, Pf);
    nbsimarray(i)=NbSim;
    relerr(i)=abs(PfExact-Pf)/PfExact;
end
h=scf();
plot(nbsimarray,relerr,"r*-")
h.children.log_flags="lln";
xtitle("Test R-S","Number of samples","Relative error");

//
// 12. Polynomes de chaos sur R-S
//

// Estimation de la probabilite de defaillance

// 1. Une collection de nx variables stochastiques
mprintf("Developpement en Polynomes de Chaos\n")
srvx = setrandvar_new();
rvxR = randvar_new("Normale");
setrandvar_addrandvar ( srvx, rvxR);
rvxS = randvar_new("Normale");
setrandvar_addrandvar ( srvx, rvxS);
// 2. Une collection de nx variables incertaines
srvu = setrandvar_new();
srvuR = randvar_new("Normale",muR,sigmaR);
setrandvar_addrandvar ( srvu, srvuR);
srvuS = randvar_new("Normale",muS,sigmaS);
setrandvar_addrandvar ( srvu, srvuS);
// 3. Le plan d'experiences
degre = 2;
setrandvar_buildsample(srvx,"Quadrature",degre);
setrandvar_buildsample( srvu , srvx );
// 4. Cree le polynome de chaos
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
// 5. Realise le plan d'experiences
inputdata = setrandvar_getsample(srvu);
outputdata = LimitState(inputdata);
mprintf("Number of simulations: %d\n",size(inputdata,"r"))
polychaos_settarget(pc,outputdata);
// 6. Calcule les coefficients du P.C.
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 7. Calcule un echantillon interne, par Monte-Carlo
polychaos_buildsample ( pc , "MonteCarlo" , 10^6 );
// 8. Estime P(G<0)
p = polychaos_getinvquantile ( pc , 0 );
mprintf("p=%e (exact=%e)\n",p,PfExact);
// Clean-up
nisp_destroyall();
