//
// Master M2S - Projet
// script sobol1.sce
// Jean-Marc Martinez Fevrier 2013
//

// Modele mathematique a analyser : noyaux de Dirichlet
function y = MyFunction (x , coef)
  y=1;
  for i=1:size(x,2)
    // %eps est la precison machine
    if (abs(x(i)) < %eps) then 
      a = 2 * i + 1;
    else
      a = sin((2*i+1) * %pi * x(i))/sin(%pi * x(i));
    end
    b = (a - 1) / sqrt(2 * i);
    y = y * (1 + coef(i) * b);
  end
endfunction

// Dimension et coefficients alpha
d     = 3;
 alpha = [1/2 1/3 1/4]; // examen
//alpha = [1/3 1/4 1/5];    // sujet

// Calcul de la moyenne de Y
muy = 1;

// Calcul de la variance de Y
cv2 = alpha .^2;
vay = muy^2 * (prod(cv2 + 1) - 1);

sx = zeros(d,1);
sg = zeros(d,1);
for i=1:d
  sx(i) = cv2(i) / (prod(cv2+1) - 1);
  sg(i) = sx(i)  * prod(cv2+1) / (cv2(i)+1);
end

// On regroupe les variables
srvu = setrandvar_new();
for i=1:d
  rvu(i) = randvar_new("Uniforme",0,1);
  setrandvar_addrandvar(srvu, rvu(i));
end

// Réalisation d'un échantillon via la méthode SRS
np = 5000;
setrandvar_buildsample(srvu,"MonteCarlo",np);

// Plan d'expériences : matrice A
A = setrandvar_getsample(srvu);

// Plan d'expériences : matrice B
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);

// Réalisation des plans A et B
for k=1:np
  ya(k) = MyFunction(A(k,:), alpha);
  yb(k) = MyFunction(B(k,:), alpha);
end

// Estimation des indices par la methode de Sobol
mprintf("\n");
mprintf("Analyse de sensibilité sur le modèle :\n");
mprintf("noyaux de Dirichlet (Bench Mascot-Num)\n");
mprintf("Dimension 3 \n");
mprintf("Coefficients du modèle : alpha = [%s]\n",..
    strcat(string(alpha)," , "));
mprintf("Méthode de Sobol\n");
mprintf("\ttaille des échantillons %d\n",np);
mprintf("\tnombre d''appels au modèle %d\n",np*(d+2));

for i=1:d
  C=B;
  C(:,i) = A(:,i);
  for k=1:np
    yc(k) = MyFunction(C(k,:), alpha);
  end

  // Calcul des indices
  rho   = corrcoef( ya , yc );
  rho   = min(1., max(-1. , rho));
  s1    = min(1., max(0., rho));

  rho   = corrcoef( yb , yc );
  rho   = min(1., max(-1. , rho));
  st    = max(0., 1. - rho);

  mprintf("\nSensitivity index of variable X[%d]\n",i);
  mprintf("First index is : %12.4e (exact %12.4e)\n", s1,sx(i));
  mprintf("Total index is : %12.4e (exact %12.4e)\n", st,sg(i));
end
