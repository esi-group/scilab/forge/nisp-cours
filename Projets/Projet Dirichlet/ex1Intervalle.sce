// Mod�le math�matique � analyser
// fonctions noyaux de Dirichlet (voir bench JMM Mascot Num)
function y = MyFunction (x , alpha)
  y=1;
  for i=1:size(x,2)
    // %eps est la precison machine
    if (abs(x(i)) < %eps) then 
      a = 2 * i + 1;
    else
      a = sin((2*i+1) * %pi * x(i))/sin(%pi * x(i));
    end
    b = (a - 1) / sqrt(2 * i);
    y = y * (1 + alpha(i) * b);
  end
endfunction


// Dimension 
d=3;

// Coefficients du modele (utilises par MyFunction)
for i=1:d
  alpha(i) = 1. / i;
end

// Calcul de la moyenne de Y
muy = 1;

// Calcul de la variance de Y
pr  = 1.;
for i=1:d
  pr = pr * (1 + alpha(i)^2);
end
vay = pr - 1;

// Calcul des indices du premier ordre et des indices totaux
sx=ones(d,1);
for i=1:d
  sx(i) = (alpha(i)^2) / vay;
end
sg=ones(d,1);
for i=1:d
  a     = pr/(1 + alpha(i)^2);
  sg(i) = 1 - (a-1)/vay;
end

// On regroupe les variables
srvu = setrandvar_new();
for i=1:d
  rvu(i) = randvar_new("Uniforme",0,1);
  setrandvar_addrandvar(srvu, rvu(i));
end

// R�alisation d'un �chantillon via la m�thode SRS
np = 5000;
setrandvar_buildsample(srvu,"MonteCarlo",np);

// Plan d'exp�riences : matrice A
A = setrandvar_getsample(srvu);

// Plan d'exp�riences : matrice B
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);

// R�alisation des plans A et B
for k=1:np
  ya(k) = MyFunction(A(k,:), alpha);
  yb(k) = MyFunction(B(k,:), alpha);
end

// Intervalle de confiance
niveau = 1.96/sqrt(np-3.);

// Estimation des indices par la m�thode de Sobol
ya = ya - mean(ya);
yb = yb - mean(yb);
mprintf("\nSensitivity analysis\n");
for i=1:d
  C=B;
  C(:,i) = A(:,i);
  for k=1:np
    yc(k) = MyFunction(C(k,:), alpha);
  end
  yc = yc - mean(yc);

  // Calcul des intervalles de confiance
  rho   = (ya' * yc / (np-1)) / sqrt(variance(ya)*variance(yc));
  z     = 0.5 * log((1+rho)/(1-rho));
  s1    = max(0., rho);
  s1min = max(0., tanh(z - niveau));
  s1max = max(0., tanh(z + niveau));

  rho   = (yb' * yc / (np-1)) / sqrt(variance(yb)*variance(yc));
  z     = 0.5 * log((1+rho)/(1-rho));
  st    = max(0., 1. - rho);
  st    = min(st, 1);
  stmax = max(0., 1. - tanh(z - niveau));
  stmin = max(0., 1. - tanh(z + niveau));

  mprintf("\nSensitivity index of variable X[%d]\n",i);
  mprintf("First index is : %12.4e - Intervalle de confiance � 95%% [%12.4e %12.4e] (expected %12.4e)\n", s1,s1min,s1max,sx(i));
  mprintf("Total index is : %12.4e - Intervalle de confiance � 95%% [%12.4e %12.4e] (expected %12.4e)\n", st,stmin,stmax,sg(i));
end


