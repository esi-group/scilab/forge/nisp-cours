//
// Master M2S - Projet
// script PChaos.sce
// Jean-Marc Martinez Fevrier 2013
//

// Modele mathematique a analyser : fonction produit
function y = MyFunction (x , coef)
  y=1;
  for i=1:size(x,2)
    if (abs(x(i)) < %eps) then // %eps est la precison machine
      a = 2 * i + 1;
    else
      a = sin((2*i+1) * %pi * x(i))/sin(%pi * x(i));
    end
    b = (a - 1) / sqrt(2 * i);
    y = y * (1 + coef(i) * b);
  end
endfunction

// Dimension 
d=3;
alpha = [1/2 1/3 1/4]; // examen
//alpha = [1/3 1/4 1/5];    // sujet

// Calcul de la moyenne de Y
muy = 1;

// Calcul de la variance de Y
cv2 = alpha .^2;
vay = muy^2 * (prod(cv2 + 1) - 1);

sx = zeros(d,1);
sg = zeros(d,1);
for i=1:d
  sx(i) = cv2(i) / (prod(cv2+1) - 1);
  sg(i) = sx(i)  * prod(cv2+1) / (cv2(i)+1);
end


nx=d;
srvx = setrandvar_new();
for i=1:d
  rvx(i) = randvar_new("Uniforme");
  setrandvar_addrandvar(srvx, rvx(i));
end

// On verifie en editant sur la console 
// les parametres des variables X(i)
setrandvar_getlog(srvx);

// Specification d'un plan : quadrature tensorisee partielle
// formule exacte pour un polynome de degre = degre
degre = 12;
setrandvar_buildsample( srvx, "Petras", degre);

// Polynome de chaos
noutput = 1;
pc = polychaos_new ( srvx , noutput );
 
// Realisation du plan d'experiences numeriques
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
nx = polychaos_getdiminput(pc);
ny = polychaos_getdimoutput(pc);
inputdata  = zeros(nx);
outputdata = zeros(ny);
for k=1:np
  inputdata  = setrandvar_getsample(srvx,k);
  outputdata = MyFunction(inputdata, alpha);
  polychaos_settarget(pc,k,outputdata);
end

// Calcul des coefficients par integration numerique
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");

// Edition de l'analyse de sensibilite
mprintf("\n");
mprintf("Analyse de sensibilite sur le modele :\n");
mprintf("noyaux de Dirichlet (Bench Mascot-Num)\n");
mprintf("Dimension 3 \n");
mprintf("Coefficients du modele : alpha = [%s]\n",..
    strcat(string(alpha)," , "));
mprintf("Methode par developpement en polyn�mes de chaos\n");
mprintf("degre : %d\n",degre);
mprintf("Quadrature par tensorisation partielle\n");
mprintf("\tpolyn�mes de Legendre\n");
mprintf("\tmethode de Petras\n");

mprintf("Nombre de simulations %d\n",..
    setrandvar_getsize(srvx));
mprintf("Mean        = %f (expected %f) \n",..
    polychaos_getmean(pc),muy);
mprintf("Variance    = %f (expected %f) \n",..
    polychaos_getvariance(pc),vay);
mprintf("Indice de sensibilite du 1er ordre\n");
for i=1:d
  mprintf("    Variable X(%d) = %f (expected %f) \n",..
    i,polychaos_getindexfirst(pc,i),sx(i));
end
mprintf("Indice de sensibilite Totale\n");
for i=1:nx
  mprintf("    Variable X(%d) = %f (expected %f) \n",..
    i,polychaos_getindextotal(pc,i),sg(i));
end
