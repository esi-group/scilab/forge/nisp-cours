// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Modelisation et Simulation (M2S)
// Module Informatique scientifique approfondie (I1)
// Traitement des incertitudes (I1C)
// jean-marc.martinez@cea.fr
// michael.baudin@edf.fr
// Examen : 2012 - 2013
// Projet Crue
// Solution

// Reference
// "Revue sur l'analyse de sensibilite globale 
// de modeles numeriques", 
// B. Iooss, 
// Journal de la Societe Francaise de Statistique, 
// Vol. 152 No. 1 (2011).

function S = surverse(x)
    Q=x(:,1) // Debit (m3/s)
    Ks=x(:,2) // Coeff. Mannning-Strickler (m^(1/3)/s)
    Zv=x(:,3) // Cote du fond de la riviere en aval (m)
    Zm=x(:,4) // Cote du fond de la riviere en amont (m)
    Hd=x(:,5) // Hauteur de la digue (m)
    Zb=x(:,6) // Cote de la berge (m)
    L=x(:,7) // Longueur du troncon de riviere (m)
    B=x(:,8) // Largeur de la riviere (m)
    //
    pente=(Zm-Zv)./L // Si la pente est petite
    H=(Q./(Ks.*B.*sqrt(pente))).^(3/5)
    S=Zv+H-Hd-Zb
endfunction

function r = wilks(alpha,bet,n)
    // Calcule le rang r, tel que :
    // P(Y(r)>y)>bet
    // ou y le quantile de probabilite alpha, i.e.
    // P(Y<y)=alpha.
    // Si il n'y a pas assez de donnees, renvoit r=0.
    // Require: distfun
    if (n < log(1-bet)/log(alpha)) then 
        r=0;
    else
        r = distfun_binoinv(bet,n,alpha)
        r = r + 1
    end
endfunction

/////////////////////////////////////////////////////
//
// Parametres des distributions
//
Ksmu=30;
Kssigma=7.5;
//
Qmu=1013;
Qsigma=558;
//
Zma=54;
Zmb=56;
//
Zva=49;
Zvb=51;
//
// Parametres deterministes
Hd=3;
Zb=55.5;
L=5000;
B=300;

/////////////////////////////////////////////////////
//
// Verification du modele
//
// Debit maximal historique
x=[];
x(1)=3854; // Debit (m3/s)
x(2)=15; // Coeff. Mannning-Strickler (m^(1/3)/s)
x(3)=51; // Cote du fond de la riviere en aval (m)
x(4)=54; // Cote du fond de la riviere en amont (m)
x(5)=0; // Hauteur de la digue (m)
x(6)=55.5; // Cote de la berge (m)
x(7)=5000; // Longueur du troncon de riviere (m)
x(8)=300; // Largeur de la riviere (m)
x=x';
S = surverse(x)
S+Zb
// Valeur attendue : 59.4 (m)

/////////////////////////////////////////////////////
//
// Graphique de la PDF
//

h=scf();
//
x=linspace(8,53,100);
y = distfun_normpdf(x,Ksmu,Kssigma);
subplot(2,2,1);
plot(x,y);
xtitle("Ks (m^(1/3)/s)")
//
x=linspace(-100,4000,100);
y = distfun_evpdf(-x,-Qmu,Qsigma);
subplot(2,2,2);
plot(x,y);
xtitle("Q (m^3/s)")
//
x=linspace(53,57,100);
y = distfun_unifpdf(x,Zma,Zmb);
subplot(2,2,3);
plot(x,y);
xtitle("Zm (m)")
h.children(1).data_bounds(2,2)=0.6;
//
x=linspace(48,52,100);
y = distfun_unifpdf(x,Zva,Zvb);
subplot(2,2,4);
plot(x,y);
xtitle("Zv (m)")
h.children(1).data_bounds(2,2)=0.6;

/////////////////////////////////////////////////////
//
// Monte-Carlo
//
N=100000;
Ks=distfun_normrnd(Ksmu,Kssigma,N,1);
Q=-distfun_evrnd(-Qmu,Qsigma,N,1);
Zm=distfun_unifrnd(Zma,Zmb,N,1);
Zv=distfun_unifrnd(Zva,Zvb,N,1);
// Troncature:
i=find(Q<0|Ks<0);
Ks(i)=[];
Q(i)=[];
Zm(i)=[];
Zv(i)=[];
N=size(Ks,"r");
mprintf("Nombre de simulations=%d\n" , N);

//
// Histogrammes des entrees
//
scf();
//
subplot(2,2,1);
histplot(20,Ks);
xtitle("Ks (m^(1/3)/s)")
//
subplot(2,2,2);
histplot(20,Q);
xtitle("Q (m^3/s)")
//
subplot(2,2,3);
histplot(20,Zm);
xtitle("Zm (m)")
//
subplot(2,2,4);
histplot(20,Zv);
xtitle("Zv (m)")
//
x=zeros(N,8);
x(:,1)=Q; // Debit (m3/s)
x(:,2)=Ks; // Coeff. Mannning-Strickler (m^(1/3)/s)
x(:,3)=Zv; // Cote du fond de la riviere en aval (m)
x(:,4)=Zm; // Cote du fond de la riviere en amont (m)
x(:,5)=Hd; // Hauteur de la digue (m)
x(:,6)=Zb; // Cote de la berge (m)
x(:,7)=L; // Longueur du troncon de riviere (m)
x(:,8)=B; // Largeur de la riviere (m)
S = surverse(x);
//
// Scatter plot
scf();
//
subplot(2,2,1);
plot(Ks,S,"rx");
xtitle("","Ks (m^(1/3)/s)","S (m)")
//
subplot(2,2,2);
plot(Q,S,"rx");
xtitle("","Q (m^3/s)","S (m)")
//
subplot(2,2,3);
plot(Zm,S,"rx");
xtitle("","Zm (m)","S (m)")
//
subplot(2,2,4);
plot(Zv,S,"rx");
xtitle("","Zv (m)","S (m)")

// Histogramme de la surverse
scf();
histplot(20,S);
xtitle("Histogramme de la surverse","S (m)","Frequence");

/////////////////////////////////////////////////////
//
// Calcul de la probabilite de defaillance
//

// Estime Pf
failed = find(S>0);
nfail = size(failed,"*");
Pf = nfail/N;
mprintf("Nombre de crues=%d\n" , nfail);
mprintf("Pf = %e\n" , Pf);
mprintf("Période de retour = %f.3\n" , 1/Pf);

// Calcul d'un intervalle de confiance pour Pf
level=1.-0.95;
q = level/2.;
f = distfun_norminv(q,0,1,%f);
low = Pf - f * sqrt(Pf*(1.-Pf)/N);
up = Pf + f * sqrt(Pf*(1.-Pf)/N);
mprintf("95%% Int. de Conf.:[%e,%e]\n" , low,up);

/////////////////////////////////////////////////////
//
// Estimation des quantiles
//
S=gsort(S,"g","i");
m=mean(S);
mprintf("Surverse moyenne:%f (m)\n",m);

//
// Quantile a 1-0.1 (crue decenale)
i=ceil((1-0.1)*N);
mprintf("Quantile a 1-0.1:%f (m)\n",S(i));
// Quantile a 1-0.01 (crue centennale)
i=ceil((1-0.01)*N);
mprintf("Quantile a 1-0.01:%f (m)\n",S(i));
// Quantile a 1-0.001 (crue millenale)
i=ceil((1-0.001)*N);
mprintf("Quantile a 1-0.001:%f (m)\n",S(i));

// Quantile de Wilks avec confiance de 95%
i=wilks(1-0.1,0.95,N);
mprintf("Quantile a 1-0.1 (95%% de confiance):%f (m)\n",S(i));
// Quantile de Wilks avec confiance de 95%
i=wilks(1-0.01,0.95,N);
mprintf("Quantile a 1-0.01 (95%% de confiance):%f (m)\n",S(i));
// Quantile de Wilks avec confiance de 95%
i=wilks(1-0.001,0.95,N);
mprintf("Quantile a 1-0.001 (95%% de confiance):%f (m)\n",S(i));

