//
// Master M2S - Projet
// script PChaos.sce
// Jean-Marc Martinez Fevrier 2013
//

// Modele mathematique � analyser : fonction gSobol
function y = MyFunction (x , coef)
  y=1;
  for i=1:size(x,2)
    y = y * (abs(4*x(i)-2)+coef(i)) / (1 + coef(i));
  end
endfunction

// Dimension et coefficients alpha
d     = 3;
//alpha = [0 1 2]; // examen
alpha = [1 2 3];  // sujet

// Calcul de la moyenne de Y
muy = 1;

// Calcul du carre des coefficients de variation en fonction des alpha(i)
cv2 = zeros(d);
for i=1:d
  cv2(i) = 1 / (3 * (1 + alpha(i))^2);
end

// Calcul de la variance de Y en fonction des coefficients de variation
// et de la moyenne 
vay = muy^2 * (prod(cv2 + 1) - 1);

// Calcul des indices de sensibilite en fonction des 
// coefficients de variation
sx = zeros(d,1);
sg = zeros(d,1);
for i=1:d
  sx(i) = cv2(i) / (prod(cv2+1) - 1);
  sg(i) = sx(i) * prod(cv2+1) / (cv2(i)+1);
end

nx=d;
srvx = setrandvar_new();
for i=1:d
  rvx(i) = randvar_new("Uniforme");
  setrandvar_addrandvar(srvx, rvx(i));
end

// On verifie en editant sur la console les parametres des variables X(i
setrandvar_getlog(srvx);

// Specification d'un plan : quadrature tensorisee partielle
// formule exacte pour un polynome de degre = degre
degre = 20;
setrandvar_buildsample( srvx, "Petras", degre);

// Polynome de chaos
noutput = 1;
pc = polychaos_new ( srvx , noutput );
 
// Realisation du plan d'experiences numeriques
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
nx = polychaos_getdiminput(pc);
ny = polychaos_getdimoutput(pc);
inputdata  = zeros(nx);
outputdata = zeros(ny);
for k=1:np
  inputdata  = setrandvar_getsample(srvx,k);
  outputdata = MyFunction(inputdata, alpha);
  polychaos_settarget(pc,k,outputdata);
end

// Calcul des coefficients par integration numerique
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");

// Edition de l'analyse de sensibilite
mprintf("\nAnalyse de sensibilite sur le modele gSobol\n");
mprintf("Dimension 3\n");
mprintf(" - Coefficients du modele : \n");
mprintf("alpha = [%s]\n",strcat(string(alpha)," , "));
mprintf("Methode par developpement en polyn�mes de chaos\n")
mprintf("degre %d\n",degre);
mprintf("Quadrature par tensorisation partielle\n");
mprintf("\tpolyn�mes de Legendre\n");
mprintf("\tmethode de Petras\n");

mprintf("Nombre de simulations %d\n",..
    setrandvar_getsize(srvx));
mprintf("Mean        = %f (expected %f) \n",..
    polychaos_getmean(pc),muy);
mprintf("Variance    = %f (expected %f) \n",..
    polychaos_getvariance(pc),vay);
mprintf("Indice de sensibilite du 1er ordre\n");
for i=1:d
  mprintf("    Variable X(%d) = %f (expected %f) \n",..
    i,polychaos_getindexfirst(pc,i),sx(i));
end
mprintf("Indice de sensibilite Totale\n");
for i=1:nx
  mprintf("    Variable X(%d) = %f (expected %f) \n",..
    i,polychaos_getindextotal(pc,i),sg(i));
end



