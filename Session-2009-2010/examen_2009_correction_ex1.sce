// Copyright (C) 2009 - CEA - Jean-Marc Martinez
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Mod�le math�matique � analyser : fonction produit
function y = Exemple (x)
  y=1;
  for i=1:nx
    y = y * x(i);
  end
endfunction

// Dimension 
d=5;

// On sp�cifie les moyennes et les variances des X(i)
mux=[1:d]; // mux(i)=i de i=1 � d
vax=[1:d]; // vax(i)=i de i=1 � d

// Calcul de pm et pv
pv=1;
pm=1;
for i=1:d
  pv = pv * ( vax(i) + mux(i)^2 );
  pm = pm * mux(i)^2;
end

// Calcul de la moyenne de Y
muy=1;
for i=1:d
  muy=muy*mux(i);
end

// Calcul de la variance de Y
vay = pv - pm;

// Calcul des indices du premier ordre
sx=ones(d,1);
for i=1:d
  sx(i) = vax(i) * pm / (mux(i)^2 * vay);
end

// Calcul des indices totaux
st=ones(d,1);
su=ones(d,1);
for i=1:d
  su(i) =  (pv * mux(i)^2)/(vax(i) + mux(i)^2) - pm;
end
for i=1:d
  st(i) = 1 - su(i)/vay;
end

nx=d;
srvu = setrandvar_new();
srvx = setrandvar_new();
for i=1:d
  rvu(i) = randvar_new("Normale",mux(i),sqrt(vax(i)));
  setrandvar_addrandvar(srvu, rvu(i));
  rvx(i) = randvar_new("Normale");
  setrandvar_addrandvar(srvx, rvx(i));
end

// R�alisation d'un �chantillon via la m�thode LHS
np = 10000;
setrandvar_buildsample(srvu,"MonteCarlo",np);

// Plan d'exp�riences : matrice A
A = setrandvar_getsample(srvu);

// Plan d'exp�riences : matrice B
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);

// R�alisation des plans A et B
for k=1:np
  ya(k) = Exemple(A(k,:));
  yb(k) = Exemple(B(k,:));
end

// Estimation des indices par la m�thode de Sobol
yt=[ya;yb];
vt=variance(yt);
ya=ya-mean(ya);
yb=yb-mean(yb);
for i=1:nx
  C=B;
  C(:,i)=A(:,i);
  for k=1:np
    yc(k) = Exemple(C(k,:));
  end
  yc=yc-mean(yc);
  mprintf("Sensitivity index Variable X(%d)\n",i);
  mprintf("\tFirst index is : %f (expected = %f)\n",     ya' * yc / ((np-1) * vt), sx(i));
  mprintf("\tTotal index is : %f (expected = %f)\n", 1 - yb' * yc / ((np-1) * vt), st(i));
end




