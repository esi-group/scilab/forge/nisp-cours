// Copyright (C) 2009 - CEA - Jean-Marc Martinez
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Modèle mathématique à analyser
function y = Exemple (x)
  y=1;
  for i=1:nx
     y = y * x(i);
  end
endfunction
//////////////////////////////////
// Dimension
d=5;
// On spécifie les moyennes et les variances des X(i)

mux=[1:d]; // mux(i)=i de i=1 à d
vax=[1:d]; // vax(i)=i de i=1 à d
// Calcul de pm et pv
pv=1;
pm=1;
for i=1:d
  pv = pv * ( vax(i) + mux(i)^2 );
  pm = pm * mux(i)^2;
end
// Calcul de la moyenne de Y
muy = TODO
// Calcul de la variance de Y
vay = TODO
// Calcul des indices du premier ordre
sx=ones(d,1); // initialisation du vecteur s(1,...d) à 1
TODO
// Calcul des indices totaux
st=ones(d,1) // initialisation du vecteur st(1,...d) à 1
TODO
// Définition des paramètres incertains X(i) (srvu)
// et de la base stochastique (srvx)
nx=d; // dimension
srvu = setrandvar_new(); // ensemble des variables physiques
srvx = setrandvar_new(); // ensemble des variables stochastiques
for i=1:d
  rvu(i) = randvar_new("Normale",mux(i),sqrt(vax(i)));
  setrandvar_addrandvar(srvu, rvu(i));
  rvx(i) = randvar_new("Normale");
  setrandvar_addrandvar(srvx, rvx(i));
end
// On vérifie en éditant sur la console les paramètres des variables X(i)
// contenues dans l'ensemble srvu
setrandvar_getlog(srvu);
// Specification d'un plan : quadrature tensorisée
// formule excate pour un polynôme de degré = degre
degre = nx;
setrandvar_buildsample( srvx, "Quadrature", degre);
setrandvar_buildsample( srvu , srvx );
// Polynome de chaos
pc = TODO
// Réalisation du plan d'expériences numériques
TODO BY
// Calcul des coefficients par intégration numérique

polychaos_setdegree(pc, degre);
polychaos_computeexp(pc, srvx, "Integration");
// Edition de l'analyse de sensibilité
mprintf("Nombre de simulations %d\n",setrandvar_getsize(srvx));
mprintf("Mean        = %f (expected %f) \n",polychaos_getmean(pc),muy);
mprintf("Variance    = %f (expected %f) \n",polychaos_getvariance(pc),vay);
mprintf("Indice de sensibilité du 1er ordre\n");
for i=1:nx
  mprintf("    Variable X(%d) = %f (expected %f) \n",i,polychaos_getindexfirst(pc,i),sx(i));
end
mprintf("Indice de sensibilite Totale\n");
for i=1:nx
  mprintf("    Variable X(%d) = %f (expected %f) \n",i,polychaos_getindextotal(pc,i),st(i));
end

