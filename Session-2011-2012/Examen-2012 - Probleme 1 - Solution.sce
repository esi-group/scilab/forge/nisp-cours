// Master Modélisation et Simulation
// Modélisation probabiliste des incertitudes
// jean-marc.martinez@cea.fr
// Examen : 2011 - 2012

//////////////////////////////////////////////////////////////
// 1 Problème 1
// X uniforme dans [a,b]=[-5,-1]
// Y=10^X
// 1. Quelle est la loi de X?
// Y est de loi LogU(a*log(10),b*log(10))
N=10000;
a=-5;
b=-1;
X=grand(N,1,"unf",a,b);
Y=10.^X;
scf();
histplot(20,Y);
xtitle("Density of LogU(-5*log(10),-log(10))","Y","P(Y<y)");
// 2. Quelle est la médiane de Y ?
// Exact:
// m=10^P(0.5)
// avec P la fonction de répartition inverse 
// de X.
m = 1.e-3;
// Par simulation:
mprintf("Mediane:\n")
for N=10.^(2:6);
    X=grand(N,1,"unf",-5,-1);
    Y=10.^X;
    Y=gsort(Y);
    m = Y(N/2);
    mprintf("N=%d, m=%f\n",N,m)
end
// 3. Intervalle de confiance à 95% de Y ?
// [lo,hi] avec
// lo = 10^InvF(level/2)
// hi = 10^InvF(1-level/2)
// avec InvF la fonction de répartition inverse 
// de X:
// InvF(p)=a+(b-a)*p
level = 0.05;
low= 10^(a+(b-a)*(level/2));
hi= 10^(a+(b-a)*(1-level/2));
// Par simulation:
mprintf("Intervalle de Confiance à 95%%:\n")
mprintf("[%f,%f]:\n",low,hi)
for N=10.^(2:6);
    X=grand(N,1,"unf",-5,-1);
    Y=10.^X;
    i=find(low<Y&Y<hi);
    p=size(i,"*")/N;
    mprintf("N=%d, p=%f\n",N,p)
end

