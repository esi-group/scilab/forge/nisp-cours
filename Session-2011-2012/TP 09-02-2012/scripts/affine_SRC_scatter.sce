// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// Consider an affine model Y, where the inputs X_i are normal random variables.
// Create the scatter plots.
// Compute the Standardized Regression Coefficients.
//

function y = Exemple (x)
  // Returns the output y of an affine function.
  // Parameters
  // x: a np-by-nx matrix of doubles, where np is the number of experiments, and nx=2.
  // y: a np-by-1 matrix of doubles
  y(:) = x(:,1) + x(:,2) + x(:,3) + x(:,4)
endfunction

function r = lincorrcoef ( x , y )
    // Returns the linear correlation coefficient of x and y.
    // The variables are expected to be column matrices with the same size.
    x = x(:)
    y = y(:)
    x = x - mean(x)
    y = y - mean(y)
    sx = sqrt(sum(x.^2))
    sy = sqrt(sum(y.^2))
    r = x'*y / sx / sy
endfunction

// Initialisation de la graine aleatoire
nisp_initseed ( 0 );

// Create the random variables.
rvu1 = randvar_new("Normale",0,1);
rvu2 = randvar_new("Normale",0,2);
rvu3 = randvar_new("Normale",0,3);
rvu4 = randvar_new("Normale",0,4);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);
setrandvar_addrandvar ( srvu, rvu4);
// Create a sampling by a Latin Hypercube Sampling with size 5000.
nbshots = 5000;
setrandvar_buildsample(srvu, "Lhs",nbshots);
sampling = setrandvar_getsample(srvu);
// Perform the experiments.
y=Exemple(sampling);
// Scatter plots : y depending on X_i
for k=1:4
  scf();
  plot(y,sampling(:,k),'rx');
  xistr="X"+string(k);
  xtitle("Scatter plot for "+xistr,xistr,"Y");
end
// Compute the sample linear correlation coefficients
rho1 = lincorrcoef ( sampling(:,1) , y );
SRC1 = rho1^2;
SRC1expected = 1/30;
mprintf("SRC_1=%.5f (expected=%.5f)\n",SRC1,SRC1expected);
//
rho2 = lincorrcoef ( sampling(:,2) , y );
SRC2 = rho2^2;
SRC2expected = 4/30;
mprintf("SRC_2=%.5f (expected=%.5f)\n",SRC2,SRC2expected);
//
rho3 = lincorrcoef ( sampling(:,3) , y );
SRC3 = rho3^2;
SRC3expected = 9/30;
mprintf("SRC_3=%.5f (expected=%.5f)\n",SRC3,SRC3expected);
//
rho4 = lincorrcoef ( sampling(:,4) , y );
SRC4 = rho4^2;
SRC4expected = 16/30;
mprintf("SRC_4=%.5f (expected=%.5f)\n",SRC4,SRC4expected);
//
SUM = SRC1 + SRC2 + SRC3 + SRC4;
SUMexpected = 1;
mprintf("SUM=%.5f (expected=%.5f)\n",SUM,SUMexpected);

//
// Clean-up
randvar_destroy(rvu1);
randvar_destroy(rvu2);
randvar_destroy(rvu3);
randvar_destroy(rvu4);
setrandvar_destroy(srvu);

