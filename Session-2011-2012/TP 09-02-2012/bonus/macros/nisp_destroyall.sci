// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2010 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function nisp_destroyall ( )
  // Destroy all current objects.
  //
  // Calling Sequence
  //   nisp_destroyall ( )
  //
  // Description
  //   This function allows to stop all fast sequences. This 
  //   might be handy in case of interactive plouf and may allow
  //   to reset all "lost" sequences.
  //
  // Examples
  //   nisp_destroyall ( )
  //
  //   // Example of what can go wrong...
  //   // We create a first set of random variables.
  //   srvx = setrandvar_new( 3 );
  //   // We overwrite the first set, with the same variable.
  //   srvx = setrandvar_new( 3 );
  //   // We have lost one setrandvar. 
  //   // We could recover it with 
  //   setrandvar_tokens()
  //   // To destroy all the lost object, we can as well save 
  //   // some time and destroy them all at once.
  //   nisp_destroyall ( )
  //   // There is no more setrandvar in use.
  //   setrandvar_tokens() // Must be []
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin

  for obj = randvar_tokens ()
    randvar_destroy ( obj )
  end
  for obj = setrandvar_tokens ()
    setrandvar_destroy ( obj )
  end
  for obj = polychaos_tokens ()
    polychaos_destroy ( obj )
  end
endfunction

