// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function p = nisp_exppdf ( x , lambda )
  // Computes the Exponential PDF.
  //
  // Calling Sequence
  //   p = nisp_exppdf ( x , lambda )
  //
  // Parameters
  // x: a matrix of doubles
  // lambda : a matrix of doubles
  // p: a matrix of doubles, the probability
  //
  // Description
  //   This function computes the Exponential PDF.
  // This function is vectorized but all the input arguments must have the same size.
  //
  // The exponential distribution with rate lambda has density
  //
  // <latex>
  // \begin{eqnarray}
  // f(x) = \lambda e^{- \lambda x}
  // \end{eqnarray}
  // </latex>
  //
  // for x >= 0.
  // 
  // TODO : improve implementation (check arguments, check inf, nan, etc...)
  //
  // Examples
  // // http://en.wikipedia.org/wiki/Exponential_distribution
  // scf();
  // x = linspace(0,5,1000);
  // p = nisp_exppdf ( x , 0.5 );
  // plot(x,p, "r-" );
  // p = nisp_exppdf ( x , 1 );
  // plot(x,p, "m-" );
  // p = nisp_exppdf ( x , 1.5 );
  // plot(x,p, "c-" );
  // xtitle("Exponential Probability Distribution Function","X","P(X)");
  // legend(["lambda=0.5","lambda=","lambda=1.5"]);
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //
  // Bibliography
  // Wikipedia, Exponential distribution function, http://en.wikipedia.org/wiki/Exponential_distribution

  p = lambda.*exp(-lambda.*x)
endfunction

