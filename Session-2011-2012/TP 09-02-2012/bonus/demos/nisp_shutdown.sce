//
// This help file was automatically generated from nisp_shutdown.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_shutdown.sci
//

nisp_startup ( )
nisp_shutdown ( )
nisp_startup ( )
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_shutdown.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
