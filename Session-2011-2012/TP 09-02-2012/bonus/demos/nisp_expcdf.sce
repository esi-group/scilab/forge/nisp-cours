//
// This help file was automatically generated from nisp_expcdf.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_expcdf.sci
//

// http://en.wikipedia.org/wiki/Exponential_distribution
scf();
x = linspace(0,5,1000);
p = nisp_expcdf ( x , 0.5 );
plot(x,p, "r-" );
p = nisp_expcdf ( x , 1 );
plot(x,p, "m-" );
p = nisp_expcdf ( x , 1.5 );
plot(x,p, "c-" );
xtitle("Exponential Cumulated Distribution Function","X","P(X)");
legend(["lambda=0.5","lambda=","lambda=1.5"]);
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_expcdf.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
