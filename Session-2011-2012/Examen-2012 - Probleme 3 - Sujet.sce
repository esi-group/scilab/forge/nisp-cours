// Copyright (C) 2009 - CEA - Jean-Marc Martinez
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Modélisation et Simulation
// Modélisation probabiliste des incertitudes
// jean-marc.martinez@cea.fr
// Examen : 2011 - 2012

// 4.1 Méthode par simulation

// Modéle mathématique : noyaux de Dirichlet
function y = MyFunction (x , coef)
    y=1;
    for i=1:size(x,2)
        if (x(i) == 0) then
            a = 2 * i + 1;
        else
            a = sin((2*i+1) * %pi * x(i))/sin(%pi * x(i));
        end
        b = (a - 1) / sqrt(2 * i);
        y = y * (1 + coef(i) * b);
    end
endfunction
// Dimension et coefficients alpha
d = 3;
alpha = [1/2 1/3 1/4];
/////////////////////////////////////////////
// Valeurs calculées dans la partie théorique
// Moyenne de Y
muy = TODO
2
// Variance de Y
vay = TODO
// Indices de sensibilité du premier ordre
// indices de sensibilité totale
sx = TODO
st = TODO
//////////////////////////////////////////////
// Groupe de variables aléatoires
srvu = setrandvar_new();
for i=1:d
    rvu(i) = randvar_new("Uniforme",0,1);
    setrandvar_addrandvar(srvu, rvu(i));
end
// Réalisation des 2 échantillons (tirages aléatoires Monte Carlo)
// Plans d'expériences : matrices A et B
np = 5000;
setrandvar_buildsample(srvu,"MonteCarlo",np);
A = setrandvar_getsample(srvu);
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);
// Réalisation des plans d'expériences A et B par appel au modèle
for k=1:np
    ya(k) = MyFunction(A(k,:), alpha);
    yb(k) = MyFunction(B(k,:), alpha);
end
// Estimation des indices par la méthode de Sobol
mprintf("\nSensitivity analysis\n");
for i=1:d
    C=B;
    C(:,i) = A(:,i);
    TODO
    mprintf("\nSensitivity index of variable X[%d]\n",i);
    mprintf("First index is : %12.4e - (expected %12.4e)\n", s1, sx(i));
    mprintf("Total index is : %12.4e - (expected %12.4e)\n", st, sg(i));
end

// ////////////////////////////////////////////////////////////
// 4.3 Polynôme de chaos

// Modèle mathématique à analyser : fonction produit
function y = MyFunction (x , coef)
    y=1;
    for i=1:size(x,2)
        // %eps est la precison machine
        if (x(i) == 0) then
            a = 2 * i + 1;
        else
            a = sin((2*i+1) * %pi * x(i))/sin(%pi * x(i));
        end
        b = (a - 1) / sqrt(2 * i);
        y = y * (1 + coef(i) * b);
    end
endfunction
// Dimension et coefficients alpha
d = 3;
alpha = [1/2 1/3 1/4];
/////////////////////////////////////////////
// Valeurs calculées dans la partie théorique
// Moyenne de Y
muy = TODO
// Variance de Y
vay = TODO
// Indices de sensibilité du premier ordre
// indices de sensibilité totale
sx = TODO
st = TODO
////////////////////////////////////////////////
// Par defaut, Nisp cree un groupe de d variables aleatoires uniforme [0,1]
srvx = setrandvar_new(d);
// Specification d'un plan : quadrature tensorisée
// formule exacte pour un polynôme de degré <= degre
degre = 6;
setrandvar_buildsample( srvx, "Quadrature", degre);
// Polynome de chaos
noutput = 1;
pc = polychaos_new ( srvx , noutput );
// Réalisation du plan d'expériences numériques
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
nx = polychaos_getdiminput(pc);
ny = polychaos_getdimoutput(pc);
inputdata = zeros(nx);
4
outputdata = zeros(ny);
for k=1:np
    inputdata = setrandvar_getsample(srvx,k);
    outputdata = MyFunction(inputdata, alpha);
    polychaos_settarget(pc,k,outputdata);
end
// Calcul des coefficients par intégration numérique
// rappel degre <= niveau de la formule d'integration
polychaos_setdegree(pc, degre);
polychaos_computeexp(pc,srvx,"Integration");
// Edition des indices de sensibilité du premier ordre
// et des indices de sensibilite totale et rappel des valeurs exactes
TODO
