// Copyright (C) 2009-2011 - INRIA - Michael Baudin


// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// A Scilab port of the AxialStressedBeam.py demo of Open Turns.
// http://trac.openturns.org/wiki/ExampleAxialStressedBeam
// 
// R "Yield strength PDF" : LogNormale(300,30)
// F "Traction load PDF" : Normale(75000,5000)
//

//
// normpdf --
//   Computes the Normal probability distribution function
// Arguments
//   x : the outcome
//   mu : the mean (default 0)
//   sigma : the standard deviation (default 1)
//   p : the probability
// Calling sequences
//   p = normpdf ( x , mu , sigma )
//
function p = normpdf ( x , mu , sigma )
    [lhs,rhs]=argn();
    if ( ( rhs <> 3 ) ) then
        errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while %d or %d are expected."), "normpdf" , rhs , 1 , 3 );
        error(errmsg)
    end
    y = ( x - mu ) ./ sigma;
    p = exp ( - 0.5 .* y .* y )  ./ ( sigma .* sqrt ( 2.0 * %pi ) );
endfunction

// Random generator initialization:
//nisp_initseed(0)

// Analytical model definition:
function G = LimitState ( x )
    R = x(:,1)
    F = x(:,2)
    S = F./(%pi.*100)
    G = R-S
endfunction

// Test of the limit state function:
x = [300 75000];
mprintf ("x=[%s]\n" , strcat ( string ( x ) , " " ) );
mprintf ("G(x)=%f\n" , LimitState(x) );

// Initialization of the distribution collection:
srvu = setrandvar_new ( );

// Create a first marginal : 
// parameterized by its mean (mu1) and standard deviation (sigma1)
// R : LogNormale(300,30)
muX = 300;
sigmaX = 30;
sigma = sqrt(log(1+sigmaX^2/muX^2))
mu = log(muX) - 0.5*sigma^2
vu1 = randvar_new("LogNormale" , muX , sigma );
if (%t) then
    // Graphical output of the PDF
    x = linspace ( 200 , 400 , 1000 );
    p = nisp_lognormalpdf ( x , mu , sigma );
    scf();
    plot ( x , p );
    xtitle ( "Yield strength PDF" , "R" , "PDF" );
end

// Fill the first marginal 
setrandvar_addrandvar ( srvu , vu1 );

// Create a second marginal : Log-Normal distribution
// with given mean (muX) and standard deviation (sigmaX)
// F : normale(75000,5000)
mu2 = 75000;
sigma2 = 5000;
vu2 = randvar_new("Normale" , mu2 , sigma2 );
// Graphical output of the PDF
if (%t) then
    x = linspace ( 60000 , 90000 , 1000 );
    p = normpdf ( x , mu2 , sigma2 );
    scf();
    plot ( x , p );
    xtitle ( "Traction load PDF" , "F" , "PDF" );
end

// Fill the second marginal of aCollection
setrandvar_addrandvar ( srvu , vu2 );

//
// Using Monte-Carlo simulations
//
NbSim = 100000;
setrandvar_buildsample ( srvu , "MonteCarlo" , NbSim );
sampling = setrandvar_getsample ( srvu );
// Perform the analysis:
G = LimitState ( sampling );
scf();
histplot(20,G);
xtitle ( "Limit State Function" , "G" , "Frequency" );
mprintf("Min(G)=%f\n" , min(G));
mprintf("Max(G)=%f\n" , max(G));
failed = find(G<0);
nfail=size(failed,"*");
mprintf("Number of failures=%d\n" , nfail);
Pf = nfail/NbSim;
mprintf("Number of simulations=%d\n" , NbSim);
mprintf("Pf = %f\n" , Pf);
level=1.-0.95;
q = level/2.
p = 1.-q
f = cdfnor("X",0.,1.,p,q)
low = Pf - f * sqrt(Pf*(1.-Pf)/NbSim)
up = Pf + f * sqrt(Pf*(1.-Pf)/NbSim)
mprintf("95%% Conf. Int.:[%f,%f]\n" , low,up);

// Sample output:
// x=[300 75000]
// G(x)=61.267585
// Min(G)=-88.067270
// Max(G)=204.712462
// Number of failures=3520
// Number of simulations=100000
// Pf = 0.035200
// 95% Conf. Int.:[0.034058,0.036342]

// Exact computation of the failure probability
// R : Log-Normale(300,30)
muX = 300;
sigmaX = 30;
sigma1 = sqrt(log(1+sigmaX^2/muX^2))
mu1 = log(muX) - 0.5*sigma1^2
// F : Normale(75000,5000)
mu2 = 75000;
sigma2 = 5000;

function y=myg(F)
    S=F/(%pi*100)
    FR = nisp_lognormalcdf(S,mu1,sigma1)
    fF = normpdf(F,mu2,sigma2)
    y = FR*fF
endfunction
[v,err]=intg(10000,200000,myg)
mprintf("Exact Integral=%f\n" ,v);
mprintf("Error=%e\n" ,err);
