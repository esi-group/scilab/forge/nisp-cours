// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// normpdf --
//   Computes the Normal probability distribution function
// Arguments
//   x : the outcome
//   mu : the mean (default 0)
//   sigma : the standard deviation (default 1)
//   p : the probability
// Calling sequences
//   p = normpdf ( x , mu , sigma )
//
function p = normpdf ( x , mu , sigma )
    [lhs,rhs]=argn();
    if ( ( rhs <> 3 ) ) then
        errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while %d or %d are expected."), "normpdf" , rhs , 1 , 3 );
        error(errmsg)
    end
    y = ( x - mu ) ./ sigma;
    p = exp ( - 0.5 .* y .* y )  ./ ( sigma .* sqrt ( 2.0 * %pi ) );
endfunction

// A Scilab port of 
// F. Deheeger
// "Couplage mecano-fiabiliste : 2SMART, 
// méthodologie d’apprentissage stochastique en fiabilité."
// Thèse, 2008
// "IV.4.1 Validation de l'apprentissage - la methode SMART, Le cas R-S"
// p 118
//

// Analytical model definition:
function G = LimitState ( x )
    R = x(:,1)
    S = x(:,2)
    G = R-S
endfunction

// Test of the limit state function:
x = [
7 2
-6 1
8 3
];
mprintf ("G(%s)=%f\n" , string(x(:,1))+","+string(x(:,2)),LimitState(x) );

// Variable R - Normale(7,1)
muR=7;
sigmaR=1;
// Variable S - Normale(2,1)
muS=2;
sigmaS=1;

//
// Exact computation
function y=myf(s,muR,sigmaR,muS,sigmaS)
    FRs = cdfnor("PQ",s,muR,sigmaR)
    fSs = normpdf(s,muS,sigmaS)
    y = FRs*fSs
endfunction
[PfExact,err]=intg(0,10,list(myf,muR,sigmaR,muS,sigmaS));
mprintf("Exact Integral=%f\n" ,PfExact);
mprintf("Error=%e\n" ,err);

// Generate a Simple Random Sampling
NbSim=100000;
// Variable R
R=grand(NbSim,1,"nor",muR,sigmaR);
scf();
histplot(20,R)
xtitle("PDF of R","R","Frequency")
// Variable S
S=grand(NbSim,1,"nor",muS,sigmaS);
scf();
histplot(20,S)
xtitle("PDF of S","S","Frequency")
// Put the two histograms on the same plot
scf()
histplot(20,R,style=2)
histplot(20,S,style=5)
xtitle("Test R-S","","Frequency")
legend(["R","S"])
// Monte-Carlo
x=[R,S];
G = LimitState ( x );
// Histogram
scf();
histplot(20,G)
xtitle("Test R-S","R-S","Frequency")
mprintf("Number of simulations=%d\n" , NbSim);
mprintf("Min(G) = %f\n" , min(G));
mprintf("Max(G) = %f\n" , max(G));
failed = find(G<0);
nfail = size(failed,"*")
Pf = nfail/NbSim;
mprintf("Number of failures=%d\n" , nfail);
mprintf("Pf = %e\n" , Pf);
level=1.-0.95;
q = level/2.
p = 1.-q
f = cdfnor("X",0.,1.,p,q)
low = Pf - f * sqrt(Pf*(1.-Pf)/NbSim)
up = Pf + f * sqrt(Pf*(1.-Pf)/NbSim)
mprintf("95%% Conf. Int.:[%e,%e]\n" , low,up);

// Sample output:
// G(7,2)=5.000000
// G(-6,1)=-7.000000
// G(8,3)=5.000000
// Number of simulations=100000
// Min(G) = -0.838177
// Max(G) = 11.528211
// Number of failures=19
// Pf = 1.900000e-004
// 95% Conf. Int.:[1.045753e-004,2.754247e-004]
//
// Create a figure of failures
// See "Fig. IV.2 [Cas R − S]", [1], p124
scf();
succ = find(G>0);
plot(R(succ),S(succ),"bo");
failed= find(G<0);
plot(R(failed),S(failed),"rx");
xtitle("Test R-S","R","S");
legend(["Success","Failures"]);

// Convergence des estimateurs
imax=23;
stacksize("max");
nbsimarray=[];
relerr=[];
for i=1:imax
    NbSim=2^i;
    R=grand(NbSim,1,"nor",muR,sigmaR);
    S=grand(NbSim,1,"nor",muS,sigmaS);
    x=[R,S];
    G = LimitState ( x );
    failed = find(G<0);
    nfail = size(failed,"*");
    Pf = nfail/NbSim;
    mprintf("Number of simulations=%d\n" , NbSim);
    mprintf("\tNumber of failures=%d\n" , nfail);
    mprintf("\tPf = %e\n" , Pf);
    nbsimarray(i)=NbSim;
    relerr(i)=abs(PfExact-Pf)/PfExact;
end
h=scf();
plot(nbsimarray,relerr,"r*-")
h.children.log_flags="lln";
xtitle("Test R-S","Number of samples","Relative error");
