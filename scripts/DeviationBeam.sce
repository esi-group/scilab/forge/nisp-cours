// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// A Scilab port of 
// "Example 1 : deviation of a cantilever beam"
// Examples Guide
// Open TURNS version 1.0rc1
// Documentation built from package openturns-doc-February2012
// March 8, 2012
// 2005-2012 EDF - EADS - PhiMeca
// http://doc.openturns.org/openturns-1.0/openturns-doc_april2012/html/ExamplesGuide/
//
// Notes 
//
// 1. Scilab does not have the scaled Beta distribution 
// which is used in OpenTURNS.
// Hence, we simply use a "manually" scaled Beta distribution. 
//
// 2. NISP does not have a Beta distribution.
// This is why we use Scilab random number generators.

// Analytical model definition:
function G = LimitState ( x )
    E = x(:,1)
    F = x(:,2)
    L = x(:,3)
    I = x(:,4)
    G = (F.*L.^3) ./(3*E.*I)
endfunction

// Test of the limit state function:
x = [3.e7 300 2.5 4.e-6];
mprintf ("x=[%s]\n" , strcat ( string ( x ) , " " ) );
mprintf ("G(x)=%f\n" , LimitState(x) );

// Generate a Simple Random Sampling
NbSim=100000;
// Variable E
E=grand(NbSim,1,"bet",0.93,3.2);
E=(E*2+2.7)*1.e7;
// Variable F
muX=30000
sigmaX=9000
mu=log(muX)-0.5*log(1+sigmaX^2/muX^2);
sigma=sqrt(log(1+sigmaX^2/muX^2));
F=grand(NbSim,1,"nor",mu,sigma);
F=exp(F);
// Variable L
L=grand(NbSim,1,"unf",250,260);
// Variable I
I=grand(NbSim,1,"bet",2.5,4);
I=150*I+300;
// An histogram of the inputs
scf();
subplot(2,2,1)
histplot(20,E/1.e7)
xtitle("PDF of E","(*1.e7 Pa)","Frequency")
subplot(2,2,2)
histplot(20,F/10000)
xtitle("PDF of F","(*10 000 N)","Frequency")
subplot(2,2,3)
histplot(20,L)
xtitle("PDF of L","(m)","Frequency")
subplot(2,2,4)
histplot(20,I)
xtitle("PDF of I","(m4)","Frequency")
// Monte-Carlo
x=[E,F,L,I];
G = LimitState ( x );
// Histogram of the output 
scf();
histplot(20,G)
xtitle("Deviation of the beam","Deviation (m)","Frequency")
mprintf("Number of simulations=%d\n" , NbSim);
mprintf("Min(G) = %f\n" , min(G));
mprintf("Max(G) = %f\n" , max(G));
threshold=30;
failed = find(G>threshold);
nfail = size(failed,"*")
Pf = nfail/NbSim;
mprintf("Number of failures=%d\n" , nfail);
mprintf("Pf = %f\n" , Pf);
level=1.-0.95;
q = level/2.
p = 1.-q
f = cdfnor("X",0.,1.,p,q)
low = Pf - f * sqrt(Pf*(1.-Pf)/NbSim)
up = Pf + f * sqrt(Pf*(1.-Pf)/NbSim)
mprintf("95%% Conf. Int.:[%f,%f]\n" , low,up);

// Sample output:
// x=[30000000 300 2.5 0.000004]
// G(x)=13.020833
// Number of simulations=1000
// Min(G) = 3.468282
// Max(G) = 39.248303
// Number of failures=11
// Pf = 0.011000
// 95% Conf. Int.:[0.004535,0.017465]

// Convergence des estimateurs
imax=22;
stacksize("max");
nbsimarray=[];
Pf=[];
low=[];
up=[];
for i=1:imax
    NbSim=2^i;
    nbsimarray(i)=NbSim;
    E=grand(NbSim,1,"bet",0.93,3.2);
    E=(E*2+2.7)*1.e7;
    F=grand(NbSim,1,"nor",mu,sigma);
    F=exp(F);
    L=grand(NbSim,1,"unf",250,260);
    I=grand(NbSim,1,"bet",2.5,4);
    I=150*I+300;
    x=[E,F,L,I];
    G = LimitState ( x );
    threshold=30;
    failed = find(G>threshold);
    nfail = size(failed,"*");
    Pf(i) = nfail/NbSim;
    level=1.-0.95;
    q = level/2.;
    p = 1.-q;
    f = cdfnor("X",0.,1.,p,q);
    low(i) = Pf(i) - f * sqrt(Pf(i)*(1.-Pf(i))/NbSim);
    up(i) = Pf(i) + f * sqrt(Pf(i)*(1.-Pf(i))/NbSim);
    mprintf("Number of simulations=%d\n" , NbSim);
    mprintf("\tNumber of failures=%d\n" , nfail);
    mprintf("\tPf = %e\n" , Pf(i));
    mprintf("\t95%% Conf. Int.:[%e,%e]\n" , low(i),up(i));
end
h=scf();
plot(nbsimarray,Pf,"r*-")
plot(nbsimarray,low,"b-")
plot(nbsimarray,up,"b-")
h.children.log_flags="lnn";
xtitle("Beam Deviation","Number of samples","Failure probability");
legend(["Pf","95%% Lower bound","95%% Upper Bound"]);
