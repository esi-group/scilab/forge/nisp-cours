// Supposons que X ~ B(n,p).
// Alors 
// sqrt(n)(X/n-p) -> N(0,p(1-p)) quand n->INF.
//
// Reference
// David R. Hunter. Statistics 553 : Asymptotic tools, 2011. 
// Penn. State University, 
// http://sites.stat.psu.edu/~dhunter/asymp/lectures/asymp.pdf

function plotdistribution(R,n,p)
    X=distfun_binornd(n,p,[1 R])
    X=sqrt(n)*(X/n-p)
    histo(X,[],[],1);
    sigma=sqrt(p*(1-p))
    x=linspace(-3*sigma,3*sigma);
    y=distfun_normpdf(x,0,sigma);
    plot(x,y)
    strtitle=msprintf("Asymptotic distribution, n=%d, p=%.2f",n,p)
    xtitle(strtitle,"X","Density");
endfunction

R=10000;
p=0.7;
scf();
subplot(2,2,1)
plotdistribution(R,2,p)
subplot(2,2,2)
plotdistribution(R,20,p)
subplot(2,2,3)
plotdistribution(R,50,p)
subplot(2,2,4)
plotdistribution(R,100,p)

// Soit g(u)=arcsin(sqrt(u))
// Alors 
// sqrt(n)(g(X/n)-g(p)) -> N(0,1/4) quand n->INF.

function y=g(u)
    y=asin(sqrt(u))
endfunction

function plotdistributionStab(R,n,p)
    X=distfun_binornd(n,p,[1 R])
    X=sqrt(n)*(g(X/n)-g(p))
    histo(X,[],[],1);
    sigma=1/2
    x=linspace(-3*sigma,3*sigma);
    y=distfun_normpdf(x,0,sigma);
    plot(x,y)
    strtitle=msprintf("Stabilized asymptotic distribution, n=%d, p=%.2f",n,p)
    xtitle(strtitle,"X","Density");
endfunction

R=10000;
p=0.7;
scf();
subplot(2,2,1)
plotdistributionStab(R,2,p)
subplot(2,2,2)
plotdistributionStab(R,20,p)
subplot(2,2,3)
plotdistributionStab(R,50,p)
subplot(2,2,4)
plotdistributionStab(R,100,p)
