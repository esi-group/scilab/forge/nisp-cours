// Mod�le math�matique : fonction gSobol
function y = MyFunction (x , coef)
  y=1;
  for i=1:size(x,2)
    y = y * (abs(4*x(i)-2)+coef(i)) / (1 + coef(i));
  end
endfunction

// Dimension et coefficients alpha
d     = 3;
alpha = [0 1 2];

/////////////////////////////////////////////
// Valeurs calcul�es dans la partie th�orique
// 
// Calcul de la moyenne de Y
muy = TODO

// Calcul du carrr� des coefficients de variation en fonction des alpha(i)
cv2 = TODO

// Calcul de la variance de Y en fonction des coefficients de variation
// et de la moyenne 
vay = TODO

// Calcul des indices de sensibilite en fonction des coefficients de variation
sx = TODO  // premier ordre 
sg = TODO  // sensibilite totale
////////////////////////////////////////////////

// Par defaut, Nisp cree un groupe de d variables aleatoires uniforme [0,1]
srvx = setrandvar_new(d);

// Specification d'un plan : quadrature tensoris�e
// formule exacte pour un polyn�me de degr� <= degre
degre = 12;
setrandvar_buildsample( srvx, "Petras", degre);

// Polynome de chaos
noutput = 1;
pc = polychaos_new ( srvx , noutput );

// R�alisation du plan d'exp�riences num�riques
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
nx = polychaos_getdiminput(pc);
ny = polychaos_getdimoutput(pc);
inputdata  = zeros(nx);
outputdata = zeros(ny);
for k=1:np
  inputdata  = setrandvar_getsample(srvx,k);
  outputdata = MyFunction(inputdata, alpha);
  polychaos_settarget(pc,k,outputdata);
end

// Calcul des coefficients par int�gration num�rique
// rappel degre <= niveau de la formule d'integration
polychaos_setdegree(pc, degre);
polychaos_computeexp(pc,srvx,"Integration");

// Edition des indices de sensibilit� du premier ordre
// et des indices de sensibilite totale et rappel des valeurs exactes
TODO
