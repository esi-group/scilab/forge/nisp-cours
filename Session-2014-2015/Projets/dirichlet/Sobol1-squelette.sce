// Mod�le math�matique : noyaux de Dirichlet
function y = MyFunction (x , coef)
  y=1;
  for i=1:size(x,2)
    if (x(i) == 0) then 
      a = 2 * i + 1;
    else
      a = sin((2*i+1) * %pi * x(i))/sin(%pi * x(i));
    end
    b = (a - 1) / sqrt(2 * i);
    y = y * (1 + coef(i) * b);
  end
endfunction
// Dimension et coefficients alpha
d     = 3;
alpha = [1/2 1/3 1/4];
/////////////////////////////////////////////
// Calcul de la moyenne de Y
muy = TODO
// Calcul du carrr� des coefficients de variation en fonction des alpha(i)
cv2 = TODO
// Calcul de la variance de Y en fonction des coefficients de variation
// et de la moyenne 
vay = TODO
// Calcul des indices de sensibilite en fonction des coefficients de variation
sx = TODO  // premier ordre 
sg = TODO  // sensibilite totale
//////////////////////////////////////////////

// Groupe de variables al�atoires
srvu = setrandvar_new();
for i=1:d
  rvu(i) = randvar_new("Uniforme",0,1);
  setrandvar_addrandvar(srvu, rvu(i));
end
// On aurait pu d�finir plus directement loe groupe de variables srvu
// srvu = setrandvar_new(d); // groupe de d variables uniformes [0,1]

// R�alisation des 2 �chantillons (tirages al�atoires Monte Carlo)
// Plans d'exp�riences : matrices A et B
np = 5000;
setrandvar_buildsample(srvu,"MonteCarlo",np);
A = setrandvar_getsample(srvu);
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);

// R�alisation des plans d'exp�riences A et B par appel au mod�le
for k=1:np
  ya(k) = MyFunction(A(k,:), alpha);
  yb(k) = MyFunction(B(k,:), alpha);
end

// Estimation des indices par la m�thode de Sobol
mprintf("\nSensitivity analysis\n"); // � compl�ter
for i=1:d
  C=B;
  C(:,i) = A(:,i);
  s1 = TODO
  st = TODO
  mprintf("\nSensitivity index of variable X[%d]\n",i);
  mprintf("First index is : %12.4e - (expected %12.4e)\n", s1, sx(i));
  mprintf("Total index is : %12.4e - (expected %12.4e)\n", st, sg(i));
end
