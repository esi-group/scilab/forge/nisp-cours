// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// 3 random normal variables with means 4, 2, 1 and standard 
// deviations 1, 2, 4.
nx=3;
mu=[4 2 1]';
sigma=[1 2 4]';

//
// Simple Monte-Carlo
np=10000;
x=[];
for i = 1:nx
    x(:,i)=grand(np,1,"nor",mu(i),sigma(i));
end
y = nisp_product ( x );

// Histogram
scf();
histplot(20,y)
xtitle("Product","Y","Frequency")

// Scatter plot
h=scf();
subplot(1,3,1)
plot(x(:,1),y,"b.")
xtitle("","X1","Y")
subplot(1,3,2)
plot(x(:,2),y,"b.")
xtitle("","X2","Y")
subplot(1,3,3)
plot(x(:,3),y,"b.")
xtitle("","X3","Y")
h.children(1).children.children.mark_size=2;
h.children(2).children.children.mark_size=2;
h.children(3).children.children.mark_size=2;

// Exact Sensitivity Analysis
exact = nisp_productsa ( mu , sigma );

// With Sobol' method
function x=myrandgen(m, i, mu, sigma)
    x=grand(m,1,"nor",mu(i),sigma(i))
endfunction
mprintf("With Sobol'' method.\n")
n = 1000;
s = nisp_sobolsaFirst ( nisp_product , nx , myrandgen , n );
st = nisp_sobolsaTotal( nisp_product , nx , myrandgen , n );
for i=1:nx
    mprintf("S(%d)=%f (exact=%f)\n", i, s(i), exact.S(i));
    mprintf("ST(%d)=%f (exact=%f)\n", i, st(i), exact.ST(i));
end

// Analysis of the estimates

mprintf("Analysis of the estimates\n")
jmax=5;
stacksize("max");
sxerror=[];
sterror=[];
for j = 1 : jmax
    nparray(j) = 10^j;
    np=nparray(j);
    mprintf("Sample size:%d\n",np)
    s = nisp_sobolsaFirst ( nisp_product , nx , myrandgen , np );
    st = nisp_sobolsaTotal( nisp_product , nx , myrandgen , np );
    for i=1:nx
        sxerror(j,i)=assert_computedigits(S(i),exact.S(i));
        sterror(j,i)=assert_computedigits(ST(i),exact.ST(i));
    end
end
h=scf();
xtitle("","Number of samples","Number of digits")
plot(nparray,sxerror(:,1),"r-")
plot(nparray,sxerror(:,2),"g-")
plot(nparray,sxerror(:,3),"b-")
plot(nparray,sterror(:,1),"r-.")
plot(nparray,sterror(:,2),"g-.")
plot(nparray,sterror(:,3),"b-.")
legend(["S1","S2","S3","ST1","ST2","ST3"]);
h.children.log_flags="lln";

///////////////////////////////////////////////////////////////////
//
// 4. Polynomials Chaos
//

mprintf("Polynomials Chaos\n")
// 1. Stochastic variables
srvx = setrandvar_new();
for i = 1:nx
    rvx = randvar_new("Normale",0.,1.);
    setrandvar_addrandvar ( srvx, rvx);
end
// 2. Uncertain variables
srvu = setrandvar_new();
for i = 1:nx
    rvu = randvar_new("Normale",mu(i),sigma(i));
    setrandvar_addrandvar ( srvu, rvu);
end
// 3. Design of experiments
degre = 6;
setrandvar_buildsample(srvx,"Quadrature",degre);
setrandvar_buildsample( srvu , srvx );
// 4. Create the P.C.
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
// 5. Perform the D.O.E.
inputdata = setrandvar_getsample(srvu);
outputdata = nisp_product(inputdata);
polychaos_settarget(pc,outputdata);
// 6. Computes the coefficients of the P.C.
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 7. Perform the S.A.
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);
mprintf("Mean(Y)     = %f (exact=%f)\n",average,exact.expectation);
mprintf("Variance(Y) = %f(exact=%f)\n",var,exact.var);
S = polychaos_getindexfirst(pc);
for i=1:nx
    mprintf("S(%d)=%f (exact=%f)\n",i,S(i), exact.S(i));
end
ST = polychaos_getindextotal(pc);
for i=1:nx
    mprintf("ST(%d)=%f (exact=%f)\n",i,ST(i), exact.ST(i));
end
// Clean-up
nisp_destroyall();
digitss = assert_computedigits(S,exact.S);
digitsst = assert_computedigits(S,exact.S);
for i=1:nx
    mprintf("#Exact Digits S(%d):%d\n",i,floor(digitss(i)));
    mprintf("#Exact Digits ST(%d):%d\n",i,floor(digitsst(i)));
end

// Analysis of the estimates
mprintf("Analysis of the estimates\n")
sxerror=[];
sterror=[];
degremax = 10;
degreearray=[];
for degre = 1 : degremax
    degreearray(degre)=degre;
    srvx = setrandvar_new();
    for i = 1:nx
        rvx = randvar_new("Normale",0.,1.);
        setrandvar_addrandvar ( srvx, rvx);
    end
    srvu = setrandvar_new();
    for i = 1:nx
        rvu = randvar_new("Normale",mu(i),sigma(i));
        setrandvar_addrandvar ( srvu, rvu);
    end
    setrandvar_buildsample(srvx,"Quadrature",degre);
    setrandvar_buildsample( srvu , srvx );
    ny = 1;
    pc = polychaos_new ( srvx , ny );
    np = setrandvar_getsize(srvx);
    polychaos_setsizetarget(pc,np);
    inputdata = setrandvar_getsample(srvu);
    outputdata = nisp_product(inputdata);
    polychaos_settarget(pc,outputdata);
    polychaos_setdegree(pc,degre);
    polychaos_computeexp(pc,srvx,"Integration");
    S = polychaos_getindexfirst(pc);
    ST = polychaos_getindextotal(pc);
    polychaos_destroy(pc);
    setrandvar_destroy(srvu);
    setrandvar_destroy(srvx);
    for i=1:nx
        sxerror(degre,i)=assert_computedigits(S(i),exact.S(i));
        sterror(degre,i)=assert_computedigits(ST(i),exact.ST(i));
    end
end
h=scf();
xtitle("","Degree of the P.C.","Number of digits")
plot(degreearray,sxerror(:,1),"r-")
plot(degreearray,sxerror(:,2),"g-")
plot(degreearray,sxerror(:,3),"b-")
plot(degreearray,sterror(:,1),"r-.")
plot(degreearray,sterror(:,2),"g-.")
plot(degreearray,sterror(:,3),"b-.")
legend(["S1","S2","S3","ST1","ST2","ST3"]);
