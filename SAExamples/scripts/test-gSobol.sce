// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// References
// [1] "About the use of rank transformation in sensitivity
// analysis of model output"
// Andrea Saltelli, Ilya Sobol
// Reliability Engineering and System Safety, 50 (1995)
// 225-239

function y = GSobol(x,a)
    // x : a np-by-nx matrix of doubles
    // a : a 1-by-nx or nx-by-1 matrix of doubles
    np = size(x,"r") // Nombre d'experiences
    a=a(:)' // Convert into row vector
    if (np>1) then
         // Repeat the columns: a is now a np-par-nx matrix 
        a=repmat(a,np,1)
    end
    g=(abs(4*x-2)+a)./(1+a)
    y=prod(g,"c")
endfunction

nx=3;
a = [0 9 99];
disp("a")
disp(a)

// Create a figure
// Compare with [1], p 234, Fig. 10
// 
h=scf();
colors = ["r" "g" "b"];
for i=1:nx
    x = linspace(0,1,1000)';
    y=GSobol ( x , a(i) );
    plot(x,y,colors(i))
end
legend(["a=0","a=9","a=99"],"in_lower_right");
xtitle("Function g(x) for different values of a","x","g(x)")

///////////////////////////////////////////////////////////////////
//
// Exact sensitivity analysis
//

// Dimension of the problem
// Exact mean
muexact = 1;
// Exact variance
vexact = prod(1 + 1 ./(3*(1+a).^2))-1;
//
// Estimate the mean, the variance
np=10000;
x = grand(np,nx,"def");
y = GSobol ( x , a );
mu = mean(y);
v = variance(y);
mprintf("Mean(G)=%f (exact=%f)\n",mu,muexact)
mprintf("Variance(G)=%f (exact=%f)\n",v,vexact)

// Histogram
scf();
histplot(20,y)
xtitle("GSobol","Y","Frequency")

// Scatter plot
h=scf();
subplot(1,3,1)
plot(x(:,1),y,"b.")
xtitle("","X1","Y")
subplot(1,3,2)
plot(x(:,2),y,"b.")
xtitle("","X2","Y")
subplot(1,3,3)
plot(x(:,3),y,"b.")
xtitle("","X3","Y")
h.children(1).children.children.mark_size=2;
h.children(2).children.children.mark_size=2;
h.children(3).children.children.mark_size=2;

// Exact Sensitivity Analysis

// First order sensitivity indices
sxexact = 1 ./(3*(1+a).^2)/vexact;

// Total sensitivity indices
// Compute the product, for j different from i
// To do this, compute all products, then divide by i.
suexact= prod(1 + 1 ./(3*(1+a).^2));
suexact = suexact./(1 + 1 ./(3*(1+a).^2)) - 1;
stexact = 1 - suexact/vexact;

///////////////////////////////////////////////////////////////////
//
// With Sobol' method
//

mprintf("With Sobol'' method\n")
s = nisp_sobolsaFirst ( list(GSobol,a) , nx )
st = nisp_sobolsaTotal ( list(GSobol,a) , nx );
for i=1:nx
    mprintf("S(%d)=%f (exact=%f)\n", i, s(i), sxexact(i));
    mprintf("ST(%d)=%f (exact=%f)\n", i, st(i), stexact(i));
end

// Analysis of the estimates
mprintf("Analysis of the estimates\n")
jmax=5;
stacksize("max");
sxerror=[];
sterror=[];
for j = 1 : jmax
    nparray(j) = 10^j;
    np=nparray(j);
    mprintf("Sample size:%d\n",np)
    s = nisp_sobolsaFirst ( list(GSobol,a) , nx , [] , np );
    st = nisp_sobolsaTotal( list(GSobol,a) , nx , [] , np );
    for i=1:nx
        sxerror(j,i)=assert_computedigits(s(i),sxexact(i));
        sterror(j,i)=assert_computedigits(st(i),stexact(i));
    end
end

h=scf();
xtitle("","Number of samples","Number of digits")
plot(nparray,sxerror(:,1),"r-")
plot(nparray,sxerror(:,2),"g-")
plot(nparray,sxerror(:,3),"b-")
plot(nparray,sterror(:,1),"r-.")
plot(nparray,sterror(:,2),"g-.")
plot(nparray,sterror(:,3),"b-.")
legend(["S1","S2","S3","ST1","ST2","ST3"]);
h.children.log_flags="lnn";

///////////////////////////////////////////////////////////////////
//
// Polynomials Chaos 
//

// 1. A collection of nx stochastic random variables
mprintf("Polynomials Chaos\n")
// Par defaut, NISP creee des variables uniformes.
srvx = setrandvar_new(nx);
// 2. A collection of nx uncertain variables
srvu = setrandvar_new(nx);
// 3. Design of experiments
degre = 6;
setrandvar_buildsample(srvx,"Quadrature",degre);
setrandvar_buildsample( srvu , srvx );
// 4. Create the P.C.
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
// 5. Perform the D.O.E.
inputdata = setrandvar_getsample(srvu);
outputdata = GSobol(inputdata,a);
polychaos_settarget(pc,outputdata);
// 6. Compute the coefficients of the P.C.
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 7. Perform the S.A.
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);
mprintf("Mean(Y)     = %f (exact=%f)\n",average,muexact);
mprintf("Variance(Y) = %f (exact=%f)\n",var,vexact);
S = polychaos_getindexfirst(pc);
for i=1:nx
    mprintf("S(%d)=%f (exact=%f)\n",i,S(i), sxexact(i));
end
ST = polychaos_getindextotal(pc);
for i=1:nx
    mprintf("ST(%d)=%f (exact=%f)\n",i,ST(i), stexact(i));
end
nisp_destroyall();

// Analysis of the estimates
mprintf("Analysis of the estimates\n")
sxerror=[];
sterror=[];
degremax = 20;
degreearray=[];
narray=[];
for degre = 1 : degremax
    degreearray(degre)=degre;
    srvx = setrandvar_new(nx);
    srvu = setrandvar_new(nx);
    setrandvar_buildsample(srvx,"Quadrature",degre);
    inputdata = setrandvar_buildsample( srvu , srvx );
    ny = 1;
    pc = polychaos_new ( srvx , ny );
    np = setrandvar_getsize(srvx);
    polychaos_setsizetarget(pc,np);
    outputdata = GSobol(inputdata,a);
    polychaos_settarget(pc,outputdata);
    polychaos_setdegree(pc,degre);
    polychaos_computeexp(pc,srvx,"Integration");
    S = polychaos_getindexfirst(pc);
    ST = polychaos_getindextotal(pc);
    nisp_destroyall();
    for i=1:nx
        sxerror(degre,i)=assert_computedigits(S(i),sxexact(i));
        sterror(degre,i)=assert_computedigits(ST(i),stexact(i));
    end
    narray(degre)=size(inputdata,"r");
end
h=scf();
xtitle("gSobol function");
xlabel("Degree of the P.C.");
ylabel("Number of digits");
plot(degreearray,sxerror(:,1),"r-")
plot(degreearray,sxerror(:,2),"g-")
plot(degreearray,sxerror(:,3),"b-")
plot(degreearray,sterror(:,1),"r-.")
plot(degreearray,sterror(:,2),"g-.")
plot(degreearray,sterror(:,3),"b-.")
legend(["S1","S2","S3","ST1","ST2","ST3"],"in_upper_left");
h.children.log_flags="nnn";
//
h=scf();
xtitle("gSobol function");
xlabel("Sample size");
ylabel("Number of digits");
plot(narray,sxerror(:,1),"r-")
plot(narray,sxerror(:,2),"g-")
plot(narray,sxerror(:,3),"b-")
plot(narray,sterror(:,1),"r-.")
plot(narray,sterror(:,2),"g-.")
plot(narray,sterror(:,3),"b-.")
legend(["S1","S2","S3","ST1","ST2","ST3"],"in_upper_left");
h.children.log_flags="lnn";




