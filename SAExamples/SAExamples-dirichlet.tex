\section{Dirichlet}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The function}


Consider the function \cite{MartinezDirichlet2008}, associated with the 
Dirichlet kernels, defined by:
\begin{eqnarray}
\label{eq-dirichlet1}
d_i(x)=\frac{1}{\sqrt{2i}} \left( \frac{\sin((2i+1)\pi x)}{\sin(\pi x)} - 1\right)
\end{eqnarray}
where $x\in[0,1]$ and $i\geq 1$ is an integer. 
By continuity, we set:
\begin{eqnarray}
\label{eq-dirichlet2}
d_i(0)=\sqrt{2i},
\end{eqnarray}
for $i\geq 1$.

We can prove that:
\begin{eqnarray}
\int_0^1 d_i(x)dx=0, \label{eq-dirichlet3} \\
\int_0^1 d_i^2(x)dx=1. \label{eq-dirichlet4}
\end{eqnarray}

Consider the function 
\begin{eqnarray*}
g(\bX) = \prod_{i=1}^p (1+g_i(X_i)),
\end{eqnarray*}
where $X_1, X_2, ..., X_p$ are independent random variables 
uniform in $[0,1]$ and
\begin{eqnarray*}
g_i(X_i) = \alpha_i d_i(X_i).
\end{eqnarray*}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Basic results}

We first compute $E(d_i(X_i))$ and $E(d_i(X_i)^2)$. 

Since the variable $X_i$ is uniform in the interval $[0,1]$, 
the distribution function is:
\begin{eqnarray*}
f_i(x_i)=1
\end{eqnarray*}
for $x_i\in[0,1]$, and $f_i(x_i)=0$ otherwise. 
Hence, 
\begin{eqnarray*}
E(d_i(X_i)) 
&=& \int_0^1 d_i(x_i) f_i(x_i) dx_i \\
&=& \int_0^1 d_i(x_i) dx_i.
\end{eqnarray*}
The equation \ref{eq-dirichlet3} implies 
\begin{eqnarray}
E(d_i(X_i)) &=& 0. \label{eq-dirichlet8}
\end{eqnarray}
Hence, 
\begin{eqnarray*}
E(d_i(X_i)^2) 
&=& \int_0^1 d_i(x_i)^2 f_i(x_i) dx_i \\
&=& \int_0^1 d_i(x_i)^2 dx_i
\end{eqnarray*}
The equation \ref{eq-dirichlet4} implies 
\begin{eqnarray}
E(d_i(X_i)^2) &=& 1. \label{eq-dirichlet9}
\end{eqnarray}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Expectation}

We have
\begin{eqnarray*}
E(Y) 
&=& E\left(\prod_{i=1}^p (1+\alpha_i d_i(X_i))\right) \\
&=& \prod_{i=1}^p (1+E(\alpha_i d_i(X_i))) \\
&=& \prod_{i=1}^p (1+\alpha_i E(d_i(X_i))).
\end{eqnarray*}
We now plug the equality \ref{eq-dirichlet8} into the previous equation and 
get:
\begin{eqnarray}
\label{eq-dirichlet11}
E(Y) &=& 1.
\end{eqnarray}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Variance}

We have
\begin{eqnarray*}
V(Y) 
&=& E(Y^2) - E(Y)^2 \\
&=& E\left(\prod_{i=1}^p (1+\alpha_i d_i(X_i))^2\right) - 1 \\
&=& \prod_{i=1}^p E(1+\alpha_i d_i(X_i))^2 - 1 \\
&=& \prod_{i=1}^p E(1 + 2\alpha_i d_i(X_i) + \alpha_i^2 d_i(X_i)^2) - 1, \\
&=& \prod_{i=1}^p (1 + 2\alpha_i E(d_i(X_i)) + \alpha_i^2 E(d_i(X_i)^2)) - 1.
\end{eqnarray*}
We plug the equations \ref{eq-dirichlet8} and \ref{eq-dirichlet9} into the 
previous equality and get:
\begin{eqnarray}
\label{eq-dirichlet13}
V(Y) 
&=& \prod_{i=1}^p (1 + \alpha_i^2) - 1.
\end{eqnarray}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{First order sensitivity indices}

Let $x_i\in[0,1]$ be a real number. 
Then
\begin{eqnarray*}
E(Y|X_i=x_i) 
&=& (1+\alpha_i d_i(x_i)) E\left( \prod_{\substack{j=1,2,...,p\\j\neq i}} (1+\alpha_j d_j(X_j))\right), \\
&=& (1+\alpha_i d_i(x_i)) \prod_{\substack{j=1,2,...,p\\j\neq i}} E(1+\alpha_j d_j(X_j)), \\
&=& (1+\alpha_i d_i(x_i)) \prod_{\substack{j=1,2,...,p\\j\neq i}} (1+\alpha_j E(d_j(X_j))), \\
&=& (1+\alpha_i d_i(x_i)).
\end{eqnarray*}
Hence, we have to consider the variance of the random variable:
\begin{eqnarray*}
E(Y|X_i) &=& (1+\alpha_i d_i(X_i)).
\end{eqnarray*}
We have 
\begin{eqnarray}
\label{eq-dirichlet16}
V(E(Y|X_i))
&=& E(E(Y|X_i)^2) - E(E(Y|X_i))^2.
\end{eqnarray}
But the law of total expectation implies:
\begin{eqnarray*}
E(E(Y|X_i))^2 
&=& E(Y) \\
&=& 1
\end{eqnarray*}
where the last equality comes from the equation \ref{eq-dirichlet11}.
We plug the previous equality into the equation \ref{eq-dirichlet16} and 
get
\begin{eqnarray*}
V(E(Y|X_i))
&=& E((1+\alpha_i d_i(X_i))^2) - 1 \\
&=& E(1+2\alpha_i d_i(X_i) + \alpha_i^2 d_i^2(X_i)^2) - 1 \\
&=& (1+2\alpha_i E(d_i(X_i)) + \alpha_i^2 E(d_i^2(X_i)^2)) - 1 \\
&=& (1+\alpha_i^2) - 1 \\
&=& \alpha_i^2.
\end{eqnarray*}

The first order sensitivity indice is:
\begin{eqnarray*}
S_i 
&=& \frac{V(E(Y|X_i))}{V(Y)} \\
&=& \frac{\alpha_i^2}{\prod_{i=1}^p (1 + \alpha_i^2) - 1}.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Total sensitivity indices}

The total order sensitivity indice for the variable $X_i$ is:
\begin{eqnarray*}
ST_i 
&=& 1 - \frac{V(E(Y|X_{\compset{i}}))}{V(Y)},
\end{eqnarray*}
where
\begin{eqnarray*}
V(E(Y|X_{\compset{i}})) 
&=& V(E(Y|X_1,X_2,...,X_{i-1},X_{i+1},...,X_p)).
\end{eqnarray*}

Let $x_1,x_2,...,x_{i-1},x_{i+1},...,x_p\in[0,1]$ be real numbers. 
We have
\begin{eqnarray*}
&&E(Y|X_1=x_1,X_2=x_2,...,X_{i-1}=x_{i-1},X_{i+1}=x_{i+1},...,X_p=x_{p+1}) \\
&=& \prod_{\substack{j=1,2,...,p\\j\neq i}} (1+\alpha_j d_j(x_j)) E\left(1+\alpha_i d_i(X_i)\right) \\
&=& \prod_{\substack{j=1,2,...,p\\j\neq i}} (1+\alpha_j d_j(x_j)).
\end{eqnarray*}
Hence we have to consider the variance of the random variable
\begin{eqnarray*}
E(Y|X_1,X_2,...,X_{i-1},X_{i+1},...,X_p)
&=& \prod_{\substack{j=1,2,...,p\\j\neq i}} (1+\alpha_j d_j(X_j)).
\end{eqnarray*}

The variance is
\begin{eqnarray*}
V(E(Y|X_{\compset{i}})) 
&=& V(E(Y|X_1,X_2,...,X_{i-1},X_{i+1},...,X_p)) \\
&=& E(E(Y|X_1,X_2,...,X_{i-1},X_{i+1},...,X_p)^2) \\
    && - E(E(Y|X_1,X_2,...,X_{i-1},X_{i+1},...,X_p))^2 \\
&=& E(E(Y|X_1,X_2,...,X_{i-1},X_{i+1},...,X_p)^2) - E(Y)^2 \\
&=& \prod_{\substack{j=1,2,...,p\\j\neq i}} E((1+\alpha_j d_j(X_j))^2) - 1 \\
&=& \prod_{\substack{j=1,2,...,p\\j\neq i}} E(1+2\alpha_j d_j(X_j) + \alpha_j^2 d_j(X_j)^2) - 1 \\
&=& \prod_{\substack{j=1,2,...,p\\j\neq i}} (1+2\alpha_j E(d_j(X_j)) + \alpha_j^2 E(d_j(X_j)^2)) - 1 \\
&=& \prod_{\substack{j=1,2,...,p\\j\neq i}} (1+\alpha_j^2) - 1
\end{eqnarray*}

Technically, the computation is finished, since both the 
variance $V(Y)$ and the conditional variance $V(E(Y|X_{\compset{i}}))$ 
are computed. 

But there is a way of simplifying the formula. 
Indeed,
\begin{eqnarray}
\label{eq-dirichlet26}V(E(Y|X_{\compset{i}})) 
&=& \frac{\prod_{j=1}^p (1+\alpha_j^2)}{1+\alpha_i^2} - 1
\end{eqnarray}
The equation \ref{eq-dirichlet13} implies 
\begin{eqnarray*}
\prod_{i=1}^p (1 + \alpha_i^2) = 1+ V(Y).
\end{eqnarray*}
We plug the previous equation into \ref{eq-dirichlet26} and get
\begin{eqnarray*}
V(E(Y|X_{\compset{i}})) 
&=& \frac{1+V(Y)}{1+\alpha_i^2} - 1 \\
&=& \frac{1+V(Y) - 1 - \alpha_i^2}{1+\alpha_i^2}\\
&=& \frac{V(Y) - \alpha_i^2}{1+\alpha_i^2}.
\end{eqnarray*}
Hence,
\begin{eqnarray*}
ST_i
&=& 1 - \frac{1}{V(Y)}\frac{V(Y) - \alpha_i^2}{1+\alpha_i^2} \\
&=& \frac{V(Y)(1+\alpha_i^2) - V(Y) + \alpha_i^2}{V(Y)(1+\alpha_i^2)} \\
&=& \frac{V(Y) + V(Y) \alpha_i^2) - V(Y) + \alpha_i^2}{V(Y)(1+\alpha_i^2)} \\
&=& \frac{V(Y) \alpha_i^2) + \alpha_i^2}{V(Y)(1+\alpha_i^2)} \\
&=& \frac{(1+V(Y)) \alpha_i^2}{V(Y)(1+\alpha_i^2)} \\
&=& \frac{1+V(Y)}{V(Y)} \frac{\alpha_i^2}{1+\alpha_i^2}.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sobol' decomposition}

The equation \ref{eq-prodg6} implies:
\begin{eqnarray*}
g(\bx)=
&=& 1 + \sum_{\bu\neq \emptyset} h_\bu(\bx_\bu),
\end{eqnarray*}
where
\begin{eqnarray*}
h_\bu(\bx_\bu) 
&=&\prod_{i\in\bu} g_i(\bx_i) \\
&=&\prod_{i\in\bu} \alpha_i d_i(\bx_i).
\end{eqnarray*}

