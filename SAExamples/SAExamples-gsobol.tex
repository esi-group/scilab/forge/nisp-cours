\section{GSobol}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The function}

Consider the function \cite{SobolSaltelli1995} defined by:
\begin{eqnarray*}
g(\bx)= \prod_{i=1}^p g_i(x_i),
\end{eqnarray*}
where $x\in[0,1]$ and 
\begin{eqnarray*}
g_i(x_i)=\frac{|4x_i-2|+a_i}{1+a_i},
\end{eqnarray*}
where $a_i\geq 0$ are real numbers.

We consider the random variables $X_i$ independent and uniform in the 
interval $[0,1]$. 
In other words, we consider the probability distribution function 
\begin{eqnarray*}
f_i(x)=1,
\end{eqnarray*}
for $x\in[0,1]$ and $f_i(x)=0$ otherwise. 

When $a_i$ is small, then the parameter $X_i$ is more influential 
on the variability of the output $Y=g(\bX)$. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Properties}

The figure \ref{fig-gsobol} presents the behavior of $g$ for various 
values of $a$.
\begin{figure}
\begin{center}
\includegraphics[width=12cm]{figures/gsobol-giai}
\end{center}
\caption{Function $g(x)$ for different values of a.}
\label{fig-gsobol}
\end{figure}

\begin{proposition}
We have
\begin{eqnarray}
\label{eq-gsobol2-1}
\int_0^1 g_i(x_i) dx_i = 1.
\end{eqnarray}
\end{proposition}

\begin{proof}
Indeed, 
\begin{eqnarray*}
\int_0^1 g_i(x_i) dx_i 
&=& \int_0^{0.5} g_i(x_i) dx_i + \int_{0.5}^1 g_i(x_i) dx_i 
\end{eqnarray*}
The function $g_i$ is symetric with respect to $x=1/2$. 
Moreover, we have $g_i(x)\geq 0$ for any $x\in[0,1]$. 
Therefore, 
\begin{eqnarray*}
\int_{0.5}^1 g_i(x_i) dx_i
&=& \int_0^{0.5} g_i(x_i) dx_i
\end{eqnarray*}
Hence,
\begin{eqnarray}
\label{eq-gsobol2-2b}\int_0^1 g_i(x_i) dx_i 
&=& 2 \int_0^{0.5} g_i(x_i) dx_i.
\end{eqnarray}

If $x\in[0,0.5]$, we have $4x-2\leq 0$, so that
\begin{eqnarray*}
\int_0^{0.5} g_i(x_i) dx_i
&=& \int_0^{0.5} \frac{(2-4x_i) + a_i}{1+a_i} dx_i \\
\end{eqnarray*}
However, 
\begin{eqnarray*}
\int_0^{0.5} (2-4x_i) dx_i
&=& \left[ 2x_i -2x_i^2 \right]_0^{0.5} \\
&=& 2\frac{1}{2} - 2 \frac{1}{4} \\
&=& \frac{1}{2}
\end{eqnarray*}

Hence, 
\begin{eqnarray*}
\int_0^{0.5} g_i(x_i) dx_i
&=& \frac{1}{1+a_i} \int_0^{0.5} (2-4x + a_i)dx_i \\
&=& \frac{1}{1+a_i} \left( \int_0^{0.5} (2-4x_i)dx_i + a_i \int_0^{0.5} dx_i\right) \\
&=& \frac{1}{1+a_i} \left( \frac{1}{2} + a_i \frac{1}{2} \right)
\end{eqnarray*}
which implies 
\begin{eqnarray*}
\int_0^{0.5} g_i(x_i) dx_i &=& \frac{1}{2}.
\end{eqnarray*}
We plug the previous equation 
into \ref{eq-gsobol2-2b} and get \ref{eq-gsobol2-1}.
\end{proof}

\begin{proposition}
We have
\begin{eqnarray}
\label{eq-gsobolsq-1}
\int_0^1 g_i^2(x_i) dx_i = 1 + \frac{1}{3(1+a)^2}.
\end{eqnarray}
\end{proposition}

\begin{proof}
We have
\begin{eqnarray}
\label{eq-gsobolsq-2}
\int_0^1 g_i^2(x_i) dx_i 
&=& 2 \int_0^{0.5} g_i^2(x_i) dx_i,
\end{eqnarray}
since $g_i$ is symetric with respect to $x=0.5$ and nonnegative.
Therefore, we must compute:
\begin{eqnarray*}
\int_0^{0.5} g_i^2(x_i) dx_i
&=& \int_0^{0.5} \left( \frac{(2-4x)+a_i}{1+a_i} \right)^2 dx_i
\end{eqnarray*}
However,
\begin{eqnarray*}
\int_0^{0.5} (4x_i-2)^2 dx_i
&=& \int_0^{0.5} (16x_i^2 - 16x_i+4) dx_i \\
&=& \left[ \frac{16}{3} x_i^3 - 8 x_i^2 + 4x_i \right]_0^{0.5} \\
&=& \frac{16}{3} \frac{1}{8} - 8 \frac{1}{4} + 4\frac{1}{2} \\
&=& \frac{2}{3} - 2 + 2 \\
&=& \frac{2}{3}.
\end{eqnarray*}
Hence, 
\begin{eqnarray*}
\int_0^{0.5} g_i^2(x_i) dx_i
&=& \frac{1}{(1+a_i)^2} \int_0^{0.5} (2 - 4x_i+a_i)^2 dx_i \\
&=& \frac{1}{(1+a_i)^2} \left( \int_0^{0.5} (2-4x_i)^2 dx_i 
+ 2 a_i \int_0^{0.5} (2-4x_i) dx_i + a_i^2 \int_0^{0.5} dx_i \right)\\
&=& \frac{1}{(1+a_i)^2} \left( \frac{2}{3} + 2 a_i \frac{1}{2} + a_i^2 \frac{1}{2} \right)
\end{eqnarray*}
which implies 
\begin{eqnarray}
\int_0^{0.5} g_i^2(x_i) dx_i
&=& \frac{1}{(1+a_i)^2} \left( \frac{2}{3} + a_i + \frac{1}{2} a_i^2  \right)
\label{eq-gsobolsq-3b}
\end{eqnarray}We notice that
\begin{eqnarray*}
\frac{1}{2}(1+a_i)^2
&=& \frac{1}{2} + a_i + \frac{1}{2} a_i^2.
\end{eqnarray*}
But $1/2+1/6=2/3$. 
Hence, 
\begin{eqnarray*}
\frac{1}{6} + \frac{1}{2}(1+a_i)^2
&=& \frac{2}{3} + a_i + \frac{1}{2} a_i^2.
\end{eqnarray*}
We plug the previous equation into \ref{eq-gsobolsq-3b}, and get
\begin{eqnarray*}
\int_0^{0.5} g_i^2(x_i) dx_i
&=& \frac{1}{(1+a_i)^2} \left( \frac{1}{6} + \frac{1}{2}(1+a_i)^2 \right) \\
&=& \frac{1}{2} + \frac{1}{6} \frac{1}{(1+a_i)^2}.
\label{eq-gsobolsq-6b}
\end{eqnarray*}
We plug the previous equation into \ref{eq-gsobolsq-2} and get \ref{eq-gsobolsq-1}, 
which concludes the proof.
\end{proof}

\begin{proposition}
\label{prop-boundgi}
We have
\begin{eqnarray}
\label{eq-gsobol2-5}
1 - \frac{1}{1+a_i} \leq g_i(x) \leq 1+\frac{1}{1+a_i},
\end{eqnarray}
for $x\in[0,1]$.
\end{proposition}

\begin{proof}
Indeed, 
\begin{eqnarray*}
g_i(x) &=& \frac{|4x-2|-1 + 1+a_i}{1+a_i}
\end{eqnarray*}
which implies :
\begin{eqnarray}
g_i(x) &=& 1 + \frac{|4x-2|-1}{1+a_i}. \label{eq-gsobol2-6b}
\end{eqnarray}
Therefore, 
\begin{eqnarray}
\label{eq-gsobol2-7}
g_i(x) - 1
&=& \frac{|4x-2|-1}{1+a_i}.
\end{eqnarray}
We can now bound the absolute value of $|4x-2|-1$. 
We have  
\begin{eqnarray*}
|4x-2|-1 
&=& \left\{
\begin{array}{l}
(2-4x)-1 \textrm{ if } x\in[0,0.5]\\
(4x-2)-1 \textrm{ if } x\in[0.5,1].
\end{array}
\right. \\
&=& \left\{
\begin{array}{l}
1-4x \textrm{ if } x\in[0,0.5]\\
4x-3 \textrm{ if } x\in[0.5,1].
\end{array}
\right. \\
\end{eqnarray*}
On one hand, $x\in[0,0.5]$ implies $1-4x\in[-1,1]$. 
On the other hand, $x\in[0.5,1]$ implies $4x-3\in[-1,1]$. 
Therefore, $|4x-2|-1 \in[-1,1]$ for $x\in[0,1]$. 
This implies 
\begin{eqnarray*}
-\frac{1}{1+a_i} \leq \frac{|4x-2|-1}{1+a_i} \leq \frac{1}{1+a_i} 
\end{eqnarray*}
for any $a_i\geq 0$. 
We plug the previous inequality into \ref{eq-gsobol2-7} and get 
\begin{eqnarray*}
-\frac{1}{1+a_i} \leq g_i(x) - 1 \leq \frac{1}{1+a_i},
\end{eqnarray*}
which implies the equation \ref{eq-gsobol2-5} and 
concludes the proof.
\end{proof}

The proposition \ref{prop-boundgi} can be used to see the link 
between the sensitivity of the variable $X_i$ and the value of $a_i$.
\begin{itemize}
\item if $a_i=0$, then the variable 
$x_i$ is "important", since $0\leq g_i(x)\leq 2$.
\item if $a_i=9$, then the variable 
$x_i$ is "non important", since $0.90\leq g_i(x)\leq 1.10$.
\item if $a_i=99$, then the variable 
$x_i$ is "non significant", since $0.99\leq g_i(x)\leq 1.01$.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Expectation}

The equation \ref{eq-gsobol2-1} implies
\begin{eqnarray*}
E(g_i(X_i))
&=& \int_0^1 g_i(x_i) f_i(x_i)dx_i \\
&=& \int_0^1 g_i(x_i) dx_i \\
&=& 1.
\end{eqnarray*}

The expectation of $Y$ is:
\begin{eqnarray*}
E(Y)
&=& E\left(\prod_{i=1}^p g_i(x_i) \right) \\
&=& \prod_{i=1}^p E(g_i(x_i)) \\
&=& 1.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Variance}

The equation \ref{eq-gsobolsq-1} implies 
\begin{eqnarray*}
E(g_i(X_i)^2)
&=& 1 + \frac{1}{3(1+a_i)^2}.
\end{eqnarray*}

The variance of $Y$ is 
\begin{eqnarray}
\label{eq-gsobol4}
V(Y)=\prod_{i=1}^p \left(1+\frac{1}{3(1+a_i)^2}\right) -1.
\end{eqnarray}

Indeed, 
\begin{eqnarray*}
V(Y)
&=& E(Y^2) - E(Y)^2 \\
&=& E\left(\prod_{i=1}^p g_i(x_i)^2\right) - 1 \\
&=& \prod_{i=1}^p E(g_i(x_i)^2) - 1,
\end{eqnarray*}
which, combined with the equation \ref{eq-gsobolsq-1}, 
implies \ref{eq-gsobol4}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{First order sensitivity indices}

Let $x_1\in[0,1]$. 
We have 
\begin{eqnarray*}
E(Y|X_1=x_1)
&=&g_1(x_1)\prod_{i=2,3,...,p} E(g_i(X_i)) \\
&=&g_1(x_1).
\end{eqnarray*}
Therefore, we consider the random variable 
\begin{eqnarray*}
E(Y|X_1)
&=&g_1(X_1).
\end{eqnarray*}
Its variance is:
\begin{eqnarray*}
V(E(Y|X_1))
&=&E(E(Y|X_1)^2) - E(E(Y|X_1))^2 \\
&=&E(g_1(X_1)^2) - E(Y)^2 \\
&=& 1 + \frac{1}{3(1+a_1)^2} - 1 \\
&=& \frac{1}{3(1+a_1)^2}.
\end{eqnarray*}
Hence, the first order sensitivity indice for $X_1$ is:
\begin{eqnarray*}
S_1 = \frac{1}{V(Y)} \frac{1}{3(1+a_1)^2}.
\end{eqnarray*}
More generally, for any $i=1,2,...,p$, 
\begin{eqnarray*}
S_i = \frac{1}{V(Y)} \frac{1}{3(1+a_i)^2}.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Total sensitivity indices}

Let $i_1,i_2,...,i_s$ be integers in the set $\{1,2,...,p\}$. 
Let $x_{i_1},x_{i_2},...,x_{i_s}$ be real numbers in the interval $[0,1]$
We have
\begin{eqnarray*}
&& E(Y|X_{i_1}=x_{i_1},X_{i_2}=x_{i_2},...,X_{i_s}=x_{i_s}) \nonumber \\
&&= g_{i_1}(x_{i_1}) g_{i_2}(x_{i_2}) ... g_{i_s}(x_{i_s}) 
\prod_{\substack{i\in\{1,2,...,p\}\\ i\neq i_1,i_2,...,i_s}}
E(g_i(X_i)) \\
&&= g_{i_1}(x_{i_1}) g_{i_2}(x_{i_2}) ... g_{i_s}(x_{i_s}) 
\end{eqnarray*}
Hence, we consider the random variable
\begin{eqnarray*}
E(Y|X_{i_1},X_{i_2},...,X_{i_s}) 
= g_{i_1}(X_{i_1}) g_{i_2}(X_{i_2}) ... g_{i_s}(X_{i_s}) 
\end{eqnarray*}
Its variance is
\begin{eqnarray*}
V(E(Y|X_{i_1},X_{i_2},...,X_{i_s}))
&=& E(g_{i_1}(X_{i_1})^2 g_{i_2}(X_{i_2})^2 ... g_{i_s}(X_{i_s})^2) - E(Y)^2 \\ 
&=& \prod_{i=i_1,i_2,...,i_s} \left(1 + \frac{1}{3(1+a_i)^2}\right) - 1
\end{eqnarray*}

In order to compute the total order sensitivity indice $ST_i$, 
for any $i=1,2,...,p$, we must consider all the indices different from $i$. 
However, the previous equation implies 
\begin{eqnarray*}
V(E(Y|X_{\compset{i}}))
&=& \prod_{j\neq i} \left(1 + \frac{1}{3(1+a_j)^2}\right) - 1
\end{eqnarray*}
Therefore, the total order sensitivity indice is
\begin{eqnarray*}
ST_i
&=& 1 - \frac{\prod_{j\neq i} \left(1 + \frac{1}{3(1+a_j)^2}\right) - 1}{V(Y)}
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sobol' decomposition}

The equation \ref{eq-gsobol2-6b} implies 
\begin{eqnarray*}
g(\bx)
&=& \prod_{i=1}^p \left( 1 + \frac{|4x_i-2|-1}{1+a_i} \right)
\end{eqnarray*}
The proposition \ref{prop-prodg} then implies
\begin{eqnarray*}
g(\bx)
&=& 1+\sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} \frac{|4x_i-2|-1}{1+a_i} 
\end{eqnarray*}
The function $g$ can then be decomposed as
\begin{eqnarray*}
g(\bx)
&=& h_0+\sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} h_\bu(\bx_\bu),
\end{eqnarray*}
where
\begin{eqnarray*}
h_0 = 1
\end{eqnarray*}
and
\begin{eqnarray}
\label{eq-gsoboldecomp-1}h_\bu(\bx_\bu)
= \prod_{i\in\bu} \frac{|4x_i-2|-1}{1+a_i}.
\end{eqnarray}

The functions $h_\bu$ are excellent candidates for the Sobol' decomposition. 
In order to conclude, we have to prove that the functions $h_\bu$ satisfy 
the zero integral property. 

First, we have $E(Y)=h_0=1$, so that the constant in the 
decomposition is consistent with the Sobol' decomposition. 

Then, let $s$ be an integer in the set $\{1,2,\ldots,p\}$. 
Let $i_1,i_2,...,i_s$ be integers in the set $\{1,2,\ldots,p\}$ 
so that 
$$
1\leq i_1< i_2< \ldots< i_s\leq p.
$$
For any $k=1,2,\ldots,s$, we must prove that
\begin{eqnarray}
\label{eq-gsoboldecomp-0}
\int_0^1 h_{i_1,\ldots,i_s}(x_{i_1},\ldots,x_{i_s})dx_{i_k} = 0,
\end{eqnarray}
First, we notice that the equation \ref{eq-gsoboldecomp-1} is the same as
\begin{eqnarray*}
h_\bu(\bx_\bu)
= \prod_{i\in\bu} (g_i(x_i)-1).
\end{eqnarray*}
Therefore, 
\begin{eqnarray*}
\int_0^1 h_{i_1,\ldots,i_s}(x_{i_1},\ldots,x_{i_s})dx_{i_k} 
&=& \int_0^1 \prod_{i\in\{i_1,\ldots,i_s\}} (g_i(x_i)-1) dx_{i_k}.
\end{eqnarray*}
Obviously, we want to use the equation \ref{eq-gsobol2-1} 
to make the previous integral zero. 
In order to do this, we separate the indices equal to $i_k$ and 
the others. 
Indeed, 
\begin{eqnarray*}
\int_0^1 g_{i_1,\ldots,i_s}(x_{i_1},\ldots,x_{i_s})dx_{i_k} 
&=& \prod_{\substack{i\in\{i_1,\ldots,i_s\}\\ i\neq i_k}} (g_i(x_i)-1) 
\int_0^1 (g_{i_k}(x_{i_k}) -1) dx_{i_k} \\
&=& \prod_{\substack{i\in\{i_1,\ldots,i_s\}\\ i\neq i_k}} (g_i(x_i)-1) 
\left( \int_0^1 g_{i_k}(x_{i_k}) dx_{i_k} - \int_0^1 dx_{i_k} \right) \\
&=& \prod_{\substack{i\in\{i_1,\ldots,i_s\}\\ i\neq i_k}} (g_i(x_i)-1) 
\left( \int_0^1 g_{i_k}(x_{i_k}) dx_{i_k} - 1 \right).
\end{eqnarray*}
The equation \ref{eq-gsobol2-1} implies
\begin{eqnarray*}
\int_0^1 g_{i_k}(x_{i_k}) dx_{i_k}  = 1,
\end{eqnarray*}
which implies \ref{eq-gsoboldecomp-0} and concludes the proof.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Numerical experiments}


In this section we perform the sensitivity analysis of the GSobol function with 
the parameters 
$$
\ba=(0,9,99).
$$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Analysis of the function}

% \lstset{language=scilabscript}
% \lstinputlisting{scripts/test-gSobol.sce}

The following session presents the sensitivity analysis of the 
GSobol function with Martinez' method and 10000 experiments. 
\lstset{language=scilabscript}
\begin{lstlisting}
Mean(G)=1.010285 (exact=1.000000)
Variance(G)=0.335951 (exact=0.337822)
S(1)=0.986657 (exact=0.986712)
ST(1)=0.994330 (exact=0.990034)
S(2)=0.000000 (exact=0.009867)
ST(2)=0.012820 (exact=0.013157)
S(3)=0.000000 (exact=0.000099)
ST(3)=0.000130 (exact=0.000132)
\end{lstlisting}

The following session presents the sensitivity analysis of the 
GSobol function with a polynomial chaos decomposition based on a 
degree 6 polynomial. 
\lstset{language=scilabscript}
\begin{lstlisting}
Mean(Y)     = 0.967394 (exact=1.000000)
Variance(Y) = 0.394026 (exact=0.337822)
S(1)=0.986878 (exact=0.986712)
S(2)=0.009351 (exact=0.009867)
S(3)=0.000093 (exact=0.000099)
ST(1)=0.990556 (exact=0.990034)
ST(2)=0.012993 (exact=0.013157)
ST(3)=0.000130 (exact=0.000132)
\end{lstlisting}

The figure \ref{fig-functionga}present the behavior of the 
function $g(x)$ for $x\in[-1,1]$ and several values of the parameter $a$. 
The figure \ref{fig-histo} presents the empirical histogram of the output $Y$ 
when we use a simple Monte-Carlo simulation and sample with size 1000. 
The figure \ref{fig-scatterplot} presents the corresponding scatter plot. 

\begin{figure}
\begin{center}
\includegraphics[width=0.95\textwidth]{figures/gsobol-giai}
\end{center}
\caption{GSobol test case - Function $g(x)$ for different values of a.}
\label{fig-functionga}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.95\textwidth]{figures/gsobol-histo}
\end{center}
\caption{GSobol test case - Histogram of $Y$ for 1000 samples.}
\label{fig-histo}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.95\textwidth]{figures/gsobol-scatterplot.png}
\end{center}
\caption{GSobol test case - Scatter plot of $Y$ for 1000 samples.}
\label{fig-scatterplot}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Analysis of the sensitivity indices}

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/gsobol-sobolmethod}
\end{center}
\caption{GSobol test case - Martinez' method. Convergence of the sensitivity indices 
for an increasing number of simulations.}
\label{fig-sobolmethod}
\end{figure}

The figures \ref{fig-pcmethod} and \ref{fig-pcmethodsize} presents the results 
of the polynomial chaos on the gSobol function. 
The design of experiments is a tensorised Gauss-Legendre quadrature. 

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/gsobol-pcmethod}
\end{center}
\caption{GSobol test case - Polynomial chaos. Convergence of the sensitivity indices 
for an increasing number of degrees.}
\label{fig-pcmethod}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/gsobol-chaos-convergence-samplesize.pdf}
\end{center}
\caption{
GSobol test case - Polynomial chaos. 
Accuracy of the sensitivity indices vs the sample size.
}
\label{fig-pcmethodsize}
\end{figure}



From the sensitivity indices, we can rank the variables by 
decreasing order of influence to the variability of the output $Y$: 
$X_1>X_2>X_3$.
This shows that the sensitivity indices $S_i$ are decreasing 
when $i$ increases, that is to say, when $a_i$ increases. 
In other words, that, when $a_i$ is smaller, then the 
variable $X_i$ is more influential on the output $Y$. 

The difference between the first order sensitivity indices $S_i$ and 
the total sensitivity indices $ST_i$ is small, which indicates that 
the interactions between the variables do not have much influence on 
the variability of $Y$.

The histogram \ref{fig-histo} shows that the output $Y$ is 
approximately uniform, with values in the approximate range $[0,2.2]$, 
except that values closer to 2.2 tends to be rare. 

The scatter plot \ref{fig-scatterplot} shows that $X_1$ tends to be 
the most important influential variable with respect to $Y$. 
Indeed, if we fix $X_1$ to a particular value (by taking a 
vertial cross-section of the left figure of \ref{fig-scatterplot}), 
then the variability of $Y$ is greatly reduced. 
This implies that $V(Y|X_1)$ is much smaller than $V(Y)$, 
on the average. 
This implies that $S_1$ is the largest first order indice. 

The figure \ref{fig-sobolmethod} shows that the indices 
computed by Sobol' method converges when the number 
of samples increases. 
However, the accuracy for $S_2=0.009867$ and $S_3=0.000132$ is very low, since no 
significant digit is computed for these indices. 
This is probably because these indices are small in 
magnitude. 

The figure \ref{fig-pcmethod} shows that the indices 
computed by the polynomial chaos decomposition method slowly converges 
when the degree of the polynomial increases. 
However, we notice that the error is never zero. 
This is the expected result, since the function $g$ is continuous, 
but has discontinuous first derivatives, because of the absolute value 
term. 
As a result, the magnitude of the coefficients in the decomposition 
very slowly decrease. 
Therefore the polynomial has difficulties to converge to the 
gSobol function. 
