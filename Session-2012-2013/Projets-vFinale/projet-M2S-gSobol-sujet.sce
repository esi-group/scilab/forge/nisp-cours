// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Mod�lisation et Simulation (M2S)
// Module Informatique scientifique approfondie (I1)
// Traitement des incertitudes (I1C)
// jean-marc.martinez@cea.fr
// michael.baudin@edf.fr
// Examen : 2012 - 2013
// Projet GSobol
// Sujet

// References
// [1] "About the use of rank transformation in sensitivity
// analysis of model output"
// Andrea Saltelli, Ilya Sobol
// Reliability Engineering and System Safety, 50 (1995)
// 225-239

//
// 1. D�finition de la fonction GSobol
if (%f) then
    // 1.a. D�finition de la fonction GSobol (lente)
    function y = GSobol(x,a)
        // x : une matrice de doubles de taille np-par-nx
        // a : une matrice de doubles de taille 1-par-nx ou nx-par-1
        np = size(x,"r") // Nombre d'exp�riences
        nx = size(x,"c") // Nombre de variables
        y=ones(np,1)
        for i=1:np
            for j=1:nx
                y(i) = y(i) * (abs(4*x(i,j)-2)+a(j))/(1+a(j))
            end
        end
    endfunction
end
// 1.c. D�finition de la fonction GSobol (rapide)
function y = GSobol(x,a)
    np = size(x,"r") // Nombre d'exp�riences
    a=a(:)' // Transforme en vecteur colonne
    if (np>1) then
        a=repmat(a,np,1) // R�p�te les colonnes: matrice np-par-nx
    end
    g=(abs(4*x-2)+a)./(1+a)
    y=prod(g,"c")
endfunction
// 1.b. V�rification sur un cas particulier.
// np=2 exp�riences en dimension nx=3
TODO

// 1.d Cr�ation d�une figure
TODO

///////////////////////////////////////////////////////////////////
//
// 2. Estimation de la moyenne, de la variance.
//

// Dimension du probl�me
nx=3;
// Moyenne exacte
muexact = TODO
// Calcul de la variance exacte
vexact = TODO
//
// 2.a Estimation de la moyenne, de la variance
np=10000;
x = TODO;
y = TODO;
TODO
mu = TODO;
v = TODO;
mprintf("Mean(G)=%f (exact=%f)\n",mu,muexact)
mprintf("Variance(G)=%f (exact=%f)\n",v,vexact)

// 2.b Histogramme
TODO

// 2.c Scatter plot
TODO

///////////////////////////////////////////////////////////////////
//
// 3. M�thode de Sobol'
//

// 3.a Calcul des estimateurs

// Calcul des indices du premier ordre
sxexact=TODO

// Calcul des indices totaux
stexact=TODO

srvu = setrandvar_new();
srvx = setrandvar_new();
for i=1:nx
    rvu(i) = randvar_new("Uniforme",0.,1.);
    setrandvar_addrandvar(srvu, rvu(i));
    rvx(i) = randvar_new("Uniforme",0.,1.);
    setrandvar_addrandvar(srvx, rvx(i));
end

//
// Calcul des indices de sensibilit� via la m�thode de Sobol'
mprintf("Calcul des indices de sensibilit� via la m�thode de Sobol'\n")
// R�alisation d'un �chantillon via la m�thode MonteCarlo
np = 1000;
setrandvar_buildsample(srvu,"MonteCarlo",np);

// Plan d'exp�riences : matrice A
A = setrandvar_getsample(srvu);

// Plan d'exp�riences : matrice B
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);

// R�alisation des plans A et B
ya = GSobol(A,a);
yb = GSobol(B,a);

// Estimation des indices par la m�thode de Sobol
yt=[ya;yb];
vt=variance(yt);
ya=ya-mean(ya);
yb=yb-mean(yb);
for i=1:nx
    C=B;
    C(:,i)=A(:,i);
    TODO
    mprintf("Sensitivity index Variable X(%d)\n",i);
    mprintf("\tS(%d)=%f (exact=%f)\n", i, sxcomputed(i), sxexact(i));
    mprintf("\tST(%d)=%f (exact=%f)\n", i, stcomputed(i), stexact(i));
end

// 3.b. Analyse des estimateurs
mprintf("Analyse des estimateurs\n")
jmax=5;
stacksize("max");
sxerror=[];
sterror=[];
for j = 1 : jmax
    nparray(j) = 10^j;
    np=nparray(j);
    TODO
    for i=1:nx
        TODO
        sxerror(j,i)=abs(sxcomputed(i)-sxexact(i));
        sterror(j,i)=abs(stcomputed(i)-stexact(i));
    end
end
h=scf();
xtitle("","Number of samples","Absolute Error")
TODO
h.children.log_flags="lln";

///////////////////////////////////////////////////////////////////
//
// 4. Polyn�mes de chaos
//

// 4.1 Estimation des indices de sensibilit�

// 1. Une collection de nx variables stochastiques uniformes.
mprintf("D�veloppement en Polyn�mes de Chaos\n")
// Par d�faut, NISP cr��e des variables uniformes.
srvx = setrandvar_new(nx);
// 2. Une collection de nx variables incertaines uniformes.
srvu = setrandvar_new(nx);
// 3. Le plan d'exp�riences
degre = 6;
setrandvar_buildsample(srvx,"Quadrature",degre);
setrandvar_buildsample( srvu , srvx );
// 4. Cr�� le polyn�me de chaos
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
// 5. R�alise le plan d'exp�riences
inputdata = setrandvar_getsample(srvu);
outputdata = GSobol(inputdata,a);
polychaos_settarget(pc,outputdata);
// 6. Calcule les coefficients du P.C.
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 7. Fait l'analyse de sensibilit�
TODO

// 4.2 Analyse des estimateurs

// Analyse des estimateurs
mprintf("Analyse des estimateurs\n")
sxerror=[];
sterror=[];
degremax = 10;
degreearray=[];
for degre = 1 : degremax
    degreearray(degre)=degre;
    TODO
    for i=1:nx
        sxerror(degre,i)=abs(S(i)-sxexact(i));
        sterror(degre,i)=abs(ST(i)-stexact(i));
    end
end
h=scf();
xtitle("","Degree of the P.C.","Absolute Error")
TODO
h.children.log_flags="nln";
