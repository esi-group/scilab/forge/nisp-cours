// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

// Master Modélisation et Simulation (M2S)
// Module Informatique scientifique approfondie (I1)
// Traitement des incertitudes (I1C)
// jean-marc.martinez@cea.fr
// michael.baudin@edf.fr
// Examen : 2012 - 2013
// Projet GSobol
// Solution

// References
// [1] "About the use of rank transformation in sensitivity
// analysis of model output"
// Andrea Saltelli, Ilya Sobol
// Reliability Engineering and System Safety, 50 (1995)
// 225-239

///////////////////////////////////////////////////////////////////
//
// 1. Définition de la fonction GSobol
//

if (%f) then
    // 1.a. Définition de la fonction GSobol (lente)
    function y = GSobol(x,a)
        // x : une matrice de doubles de taille np-par-nx
        // a : une matrice de doubles de taille 1-par-nx ou nx-par-1
        np = size(x,"r") // Nombre d'expériences
        nx = size(x,"c") // Nombre de variables
        y=ones(np,1)
        for i=1:np
            for j=1:nx
                y(i) = y(i) * (abs(4*x(i,j)-2)+a(j))/(1+a(j))
            end
        end
    endfunction
end
// 1.c. Définition de la fonction GSobol (rapide)
function y = GSobol(x,a)
    np = size(x,"r") // Nombre d'expériences
    a=a(:)' // Transforme en vecteur colonne
    if (np>1) then
        a=repmat(a,np,1) // Répète les colonnes: matrice np-par-nx
    end
    g=(abs(4*x-2)+a)./(1+a)
    y=prod(g,"c")
endfunction
// 1.b. Vérification sur un cas particulier.
// np=2 expériences en dimension nx=3
x = [
0.0 0.0 0.0
0.1 0.8 0.5
]
a = [0 1 2]
y = GSobol(x,a)
mprintf("GSobol:")
disp("a")
disp(a)
disp("x")
disp(x)
disp("y")
disp(y)

// 1.d Création d’une figure
// Comparer avec [1], p 234, Fig. 10
// 
h=scf();
colors = ["r" "g" "b"];
for i=1:3
    x = linspace(0,1,1000)';
    y=GSobol ( x , a(i) );
    plot(x,y,colors(i))
end
legend(["a=0","a=9","a=99"],"in_lower_right");
xtitle("Function g(x) for different values of a","x","g(x)")

///////////////////////////////////////////////////////////////////
//
// 2. Estimation de la moyenne, de la variance.
//

// Dimension du problème
nx=3;
// Moyenne exacte
muexact = 1;
// Calcul de la variance exacte
vexact = 1.;
for i = 1:nx
    vexact = vexact * (1 + 1/(3*(1+a(i))^2));
end
vexact = vexact-1;
// Plus rapide:    vexact = prod(1 + 1 ./(3*(1+a).^2))-1;
//
// 2.a Estimation de la moyenne, de la variance
np=10000;
x = grand(np,nx,"def");
mprintf("a:\n")
disp(a)
y = GSobol ( x , a );
mu = mean(y);
v = variance(y);
mprintf("Mean(G)=%f (exact=%f)\n",mu,muexact)
mprintf("Variance(G)=%f (exact=%f)\n",v,vexact)

// 2.b Histogramme
scf();
histplot(20,y)
xtitle("GSobol","Y","Frequency")

// 2.c Scatter plot
h=scf();
subplot(1,3,1)
plot(x(:,1),y,"b.")
xtitle("","X1","Y")
subplot(1,3,2)
plot(x(:,2),y,"b.")
xtitle("","X2","Y")
subplot(1,3,3)
plot(x(:,3),y,"b.")
xtitle("","X3","Y")
h.children(1).children.children.mark_size=2;
h.children(2).children.children.mark_size=2;
h.children(3).children.children.mark_size=2;

///////////////////////////////////////////////////////////////////
//
// 3. Méthode de Sobol'
//

// 3.a Calcul des estimateurs

// Calcul des indices du premier ordre
sxexact=ones(nx,1);
for i=1:nx
    sxexact(i) = 1/(3*(1+a(i)).^2)/vexact;
end
// Plus rapide:    sxexact = 1 ./(3*(1+a).^2)/vexact;

// Calcul des indices totaux
stexact=ones(nx,1);
suexact=ones(nx,1);
for i=1:nx
    for j=1:nx
        if (i<>j) then
            suexact(i) = suexact(i)*(1 + 1/(3*(1+a(j))^2));
        end
    end
    suexact(i)=(suexact(i)-1)/vexact;
end
for i=1:nx
    stexact(i)=1-suexact(i);
end
// Plus rapide:
// Calcule le produit,  pour j different de i.
// Pour le faire, calcule tous les produits, 
// puis divise par i.
if (%f) then
    suexact= prod(1 + 1 ./(3*(1+a).^2));
    suexact = suexact./(1 + 1 ./(3*(1+a).^2)) - 1;
    stexact = 1 - suexact/vexact;
end

srvu = setrandvar_new();
srvx = setrandvar_new();
for i=1:nx
    rvu(i) = randvar_new("Uniforme",0.,1.);
    setrandvar_addrandvar(srvu, rvu(i));
    rvx(i) = randvar_new("Uniforme",0.,1.);
    setrandvar_addrandvar(srvx, rvx(i));
end

//
// Calcul des indices de sensibilité via la méthode de Sobol'
mprintf("Calcul des indices de sensibilité via la méthode de Sobol\n")
// Réalisation d'un échantillon via la méthode MonteCarlo
np = 1000;
setrandvar_buildsample(srvu,"MonteCarlo",np);

// Plan d'expériences : matrice A
A = setrandvar_getsample(srvu);

// Plan d'expériences : matrice B
setrandvar_buildsample(srvu,"MonteCarlo",np);
B = setrandvar_getsample(srvu);

// Réalisation des plans A et B
ya = GSobol(A,a);
yb = GSobol(B,a);

// Estimation des indices par la méthode de Sobol
yt=[ya;yb];
vt=variance(yt);
ya=ya-mean(ya);
yb=yb-mean(yb);
for i=1:nx
    C=B;
    C(:,i)=A(:,i);
    yc = GSobol(C,a);
    yc=yc-mean(yc);
    mprintf("Sensitivity index Variable X(%d)\n",i);
    sxcomputed(i) = ya' * yc / ((np-1) * vt);
    mprintf("\tS(%d)=%f (exact=%f)\n", i, sxcomputed(i), sxexact(i));
    stcomputed(i)=1 - yb' * yc / ((np-1) * vt);
    mprintf("\tST(%d)=%f (exact=%f)\n", i, stcomputed(i), stexact(i));
end

//
// Calcule les indices de sensibilité du premier ordre 
// avec les fonctions du module NISP.
sxcomputed = nisp_sobolsaFirst ( list(GSobol,a) , nx )
mprintf("Estimated S:\n")
disp(sxcomputed)
stcomputed = nisp_sobolsaTotal ( list(GSobol,a) , nx );
mprintf("Estimated ST:\n")
disp(stcomputed)

// 3.b. Analyse des estimateurs

mprintf("Analyse des estimateurs\n")
jmax=5;
stacksize("max");
sxerror=[];
sterror=[];
for j = 1 : jmax
    nparray(j) = 10^j;
    np=nparray(j);
    mprintf("Sample size:%d\n",np)
    setrandvar_buildsample(srvu,"MonteCarlo",np);
    A = setrandvar_getsample(srvu);
    setrandvar_buildsample(srvu,"MonteCarlo",np);
    B = setrandvar_getsample(srvu);
    ya = GSobol(A,a);
    yb = GSobol(B,a);
    yt=[ya;yb];
    vt=variance(yt);
    ya=ya-mean(ya);
    yb=yb-mean(yb);
    for i=1:nx
        C=B;
        C(:,i)=A(:,i);
        yc = GSobol(C,a);
        yc=yc-mean(yc);
        sxcomputed(i) = ya' * yc / ((np-1) * vt);
        stcomputed(i)=1 - yb' * yc / ((np-1) * vt);
        sxerror(j,i)=abs(sxcomputed(i)-sxexact(i));
        sterror(j,i)=abs(stcomputed(i)-stexact(i));
    end
end
h=scf();
xtitle("","Number of samples","Absolute Error")
plot(nparray,sxerror(:,1),"r-")
plot(nparray,sxerror(:,2),"g-")
plot(nparray,sxerror(:,3),"b-")
plot(nparray,sterror(:,1),"r-.")
plot(nparray,sterror(:,2),"g-.")
plot(nparray,sterror(:,3),"b-.")
legend(["S1","S2","S3","ST1","ST2","ST3"]);
h.children.log_flags="lln";

///////////////////////////////////////////////////////////////////
//
// 4. Polynômes de chaos
//

// 4.1 Estimation des indices de sensibilité

// 1. Une collection de nx variables stochastiques uniformes.
mprintf("Développement en Polynômes de Chaos\n")
// Par défaut, NISP créée des variables uniformes.
srvx = setrandvar_new(nx);
// 2. Une collection de nx variables incertaines uniformes.
srvu = setrandvar_new(nx);
// 3. Le plan d'expériences
degre = 6;
setrandvar_buildsample(srvx,"Quadrature",degre);
setrandvar_buildsample( srvu , srvx );
// 4. Créé le polynôme de chaos
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
// 5. Réalise le plan d'expériences
inputdata = setrandvar_getsample(srvu);
outputdata = GSobol(inputdata,a);
polychaos_settarget(pc,outputdata);
// 6. Calcule les coefficients du P.C.
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 7. Fait l'analyse de sensibilité
average = polychaos_getmean(pc);
var = polychaos_getvariance(pc);
mprintf("Mean(Y)     = %f\n",average);
mprintf("Variance(Y) = %f\n",var);
S = polychaos_getindexfirst(pc);
for i=1:nx
    mprintf("S(%d)=%f (exact=%f)\n",i,S(i), sxexact(i));
end
ST = polychaos_getindextotal(pc);
for i=1:nx
    mprintf("ST(%d)=%f (exact=%f)\n",i,ST(i), stexact(i));
end
// Clean-up
polychaos_destroy(pc);
setrandvar_destroy(srvu);
setrandvar_destroy(srvx);

// 4.2 Analyse des estimateurs

// Analyse des estimateurs
mprintf("Analyse des estimateurs\n")
sxerror=[];
sterror=[];
degremax = 10;
degreearray=[];
for degre = 1 : degremax
    degreearray(degre)=degre;
    srvx = setrandvar_new(nx);
    srvu = setrandvar_new(nx);
    setrandvar_buildsample(srvx,"Quadrature",degre);
    setrandvar_buildsample( srvu , srvx );
    ny = 1;
    pc = polychaos_new ( srvx , ny );
    np = setrandvar_getsize(srvx);
    polychaos_setsizetarget(pc,np);
    inputdata = setrandvar_getsample(srvu);
    outputdata = GSobol(inputdata,a);
    polychaos_settarget(pc,outputdata);
    polychaos_setdegree(pc,degre);
    polychaos_computeexp(pc,srvx,"Integration");
    S = polychaos_getindexfirst(pc);
    ST = polychaos_getindextotal(pc);
    polychaos_destroy(pc);
    setrandvar_destroy(srvu);
    setrandvar_destroy(srvx);
    for i=1:nx
        sxerror(degre,i)=abs(S(i)-sxexact(i));
        sterror(degre,i)=abs(ST(i)-stexact(i));
    end
end
h=scf();
xtitle("","Degree of the P.C.","Absolute Error")
plot(degreearray,sxerror(:,1),"r-")
plot(degreearray,sxerror(:,2),"g-")
plot(degreearray,sxerror(:,3),"b-")
plot(degreearray,sterror(:,1),"r-.")
plot(degreearray,sterror(:,2),"g-.")
plot(degreearray,sterror(:,3),"b-.")
legend(["S1","S2","S3","ST1","ST2","ST3"]);
h.children.log_flags="nln";


