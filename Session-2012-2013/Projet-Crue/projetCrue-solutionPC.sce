// Copyright (C) 2013 - Michael Baudin

// This file must be used under the terms of the 
// GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

/////////////////////////////////////////////////////
//
// Polynomes de chaos sur S
//

function x=evinv(p,mu,sigma,lowertail)
    // Gumbel (minimum) inverse cumulated distribution function
    // To get the max-Gumbel Inverse CDF:
    // x=-evinv(p,-mu,sigma,lowertail)
    if (lowertail) then
        z=log(-specfun_log1p(-p))
    else
        z=log(-log(p))
    end
    x=mu+sigma*z
endfunction


// Estimation de la probabilite de defaillance
mprintf("Developpement en Polynomes de Chaos\n")
for degre = 2:10;
mprintf("Degree:%d\n",degre)
// 1. Une collection de nx variables stochastiques
srvx = setrandvar_new();
rvxQ = randvar_new("Uniforme");
rvxKs = randvar_new("Normale");
rvxZv = randvar_new("Uniforme");
rvxZm = randvar_new("Uniforme");
setrandvar_addrandvar ( srvx, rvxQ);
setrandvar_addrandvar ( srvx, rvxKs);
setrandvar_addrandvar ( srvx, rvxZv);
setrandvar_addrandvar ( srvx, rvxZm);
// 2. Une collection de nx variables incertaines
srvu = setrandvar_new();
srvuQ = randvar_new("Uniforme",0,1);
srvuKs = randvar_new("Normale",Ksmu,Kssigma);
srvuZv = randvar_new("Uniforme",Zva,Zvb);
srvuZm = randvar_new("Uniforme",Zma,Zmb);
setrandvar_addrandvar ( srvu, srvuQ);
setrandvar_addrandvar ( srvu, srvuKs);
setrandvar_addrandvar ( srvu, srvuZv);
setrandvar_addrandvar ( srvu, srvuZm);
// 3. Le plan d'experiences
setrandvar_buildsample(srvx,"Quadrature",degre);
setrandvar_buildsample( srvu , srvx );
// 4. Cree le polynome de chaos
ny = 1;
pc = polychaos_new ( srvx , ny );
np = setrandvar_getsize(srvx);
polychaos_setsizetarget(pc,np);
// 5. Realise le plan d'experiences
inputdata = setrandvar_getsample(srvu);
N=size(inputdata,"r");
QUnif = inputdata(:,1);
Ks = inputdata(:,2);
Zv = inputdata(:,3);
Zm = inputdata(:,4);
Q=-evinv(QUnif,-Qmu,Qsigma,%t);
x=zeros(N,8);
x(:,1)=Q; // Debit (m3/s)
x(:,2)=Ks; // Coeff. Mannning-Strickler (m^(1/3)/s)
x(:,3)=Zv; // Cote du fond de la riviere en aval (m)
x(:,4)=Zm; // Cote du fond de la riviere en amont (m)
x(:,5)=Hd; // Hauteur de la digue (m)
x(:,6)=Zb; // Cote de la berge (m)
x(:,7)=L; // Longueur du troncon de riviere (m)
x(:,8)=B; // Largeur de la riviere (m)
S = surverse(x);
mprintf("Number of simulations: %d\n",N)
polychaos_settarget(pc,S);
// 6. Calcule les coefficients du P.C.
polychaos_setdegree(pc,degre);
polychaos_computeexp(pc,srvx,"Integration");
// 7. Calcule un echantillon interne, par Monte-Carlo
polychaos_buildsample ( pc , "MonteCarlo" , 10^6 , 1 );
//pcS= polychaos_getsample ( pc );
//scf();
//histplot(20,pcS);
// 8. Estime P(S>0)
p = 1-polychaos_getinvquantile ( pc , 0 );
mprintf("p=%e\n",p);
// Clean-up
polychaos_destroy(pc);
setrandvar_destroy(srvu);
setrandvar_destroy(srvx);
end
