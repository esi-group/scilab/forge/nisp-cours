% Copyright (C) 2013 - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Pr�sentation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Introduction}

Nous souhaitons estimer le risque de d�bordement d'une rivi�re, 
c'est � dire �valuer un risque de crue. 
Pour cela, on utilise les �quations des �coulements � surface 
libre issus de l'hydraulique. 
L'objectif est de calculer une hauteur de surverse $S$, qui 
est n�gative lorsque la rivi�re est contenue par la digue, 
et positive lorsqu'il y a une crue. 
La figure \ref{fig-profil} pr�sente une vue d'ensemble 
de la situation de profil, tandis que les figures 
\ref{fig-coupe} et \ref{fig-coupe2} pr�sentent 
deux vues en coupe.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=8cm]{crue-profil}
\end{center}
\caption{Test Crue - Vue de profil.}
\label{fig-profil}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=8cm]{crue-coupe}
\end{center}
\caption{Test Crue - Vue en coupe (1).}
\label{fig-coupe}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=8cm]{crue-coupe2}
\end{center}
\caption{Test Crue - Vue en coupe (2).}
\label{fig-coupe2}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{D�rivation du mod�le}

Les �quations de l'hydraulique peuvent se simplifier 
lorsqu'on consid�re un �coulement stationnaire. 

Le mod�le que nous consid�rons utilise le rayon 
hydraulique, d�fini par:
\begin{eqnarray*}
R_h = \frac{A}{P}
\end{eqnarray*}
o�
\begin{itemize}
\item $A$ est la surface de la section mouill�e ($m^2$),
\item $P$ est le p�rim�tre mouill�,
\item $R_h$ est le rayon hydraulique (m).
\end{itemize}
On a:
\begin{eqnarray}
A&=&BH \label{eq-abh} \\
P&=&B+2H
\end{eqnarray}
o�
\begin{itemize}
\item $B$ est la largeur de la rivi�re (m),
\item $H$ est la hauteur d'eau maximale (m).
\end{itemize}
En cons�quence, 
\begin{eqnarray}
R_h = \frac{BH}{B+2H}.
\end{eqnarray}
On fait l'hypoth�se que la largeur de la rivi�re $B$ est 
grande par rapport � la hauteur d'eau $H$, c'est � dire que 
$B \gg H$. 

La pente $\alpha$ (en radiant) de la rivi�re est suppos�e constante, 
de telle sorte que
\begin{eqnarray}
\sin(\alpha) = \frac{Z_m-Z_v}{L}
\end{eqnarray}
o� 
\begin{itemize}
\item $Z_m$ est la c�te du fond de la rivi�re en amont (m), 
\item $Z_v$ est la c�te du fond de la rivi�re en aval (m), 
\item $L$ est la longueur du tron�on de rivi�re (m).
\end{itemize}
On fait l'hypoth�se que la pente $\alpha$ est faible. 

La formule de Manning-Strickler est un mod�le de 
la vitesse moyenne de l'eau sur une section. 
Cette �quation est:
\begin{eqnarray}
V=K_s R_h^{2/3} \sqrt{\alpha} \label{eq-maning}
\end{eqnarray} 
o� 
\begin{itemize}
\item $V$ est la vitesse moyenne de l'eau sur une coupe (m/s),
\item $K_s$ est le coefficient de Manning-Strickler de la rivi�re ($m^{1/3}$/s).
\end{itemize}

Lorsque le coefficient de Manning-Strickler $K_s$ est plus grand, 
le fond de la rivi�re s'oppose moins � l'�coulement. 
La figure \ref{fig-manningstrickler} pr�sente quelques valeurs 
du coefficient de Manning-Strickler en fonction de la nature 
du fond de la rivi�re.

\begin{figure}[htbp]
\begin{center}
\begin{tabular}{l|l}
Nature de la paroi & $K_s$ ($m^{1/3}$/s) \\
\hline
B�ton lisse & 75-90 \\
Canal en terre, non enherb� & 60 \\
Rivi�re de plaine, large, v�g�tation peu dense & 30\\
Lit majeur urbanis� & 10-15 \\
Lit majeur en for�t & <10
\end{tabular}
\end{center}
\caption{
Quelques valeurs du coefficient de Manning-Strickler. 
Adapt� de "Aide m�moire d'hydraulique � surface libre", G. Degoutte.
}
\label{fig-manningstrickler}
\end{figure}

Le d�bit de la rivi�re est approch� par
\begin{eqnarray}
Q=AV \label{eq-qav}
\end{eqnarray}
o� $Q$ est le d�bit de la rivi�re ($m^3/s$). 
Dans un probl�me de calcul de crue, la variable $Q$ est le d�bit 
annuel maximal. 

On consid�re les hauteurs suivantes:
\begin{eqnarray*}
Z_c &=& Z_v+H \\
Z_d &=& Z_b + H_d
\end{eqnarray*}
o�
\begin{itemize}
\item $Z_b$ est la c�te de la berge en aval (m).
\item $H_d$ est la hauteur de la digue (m).
\end{itemize}
La surverse est la diff�rence entre la hauteur de la crue 
et la hauteur de la digue:
\begin{eqnarray*}
S &=& Z_c - Z_d,
\end{eqnarray*}
ce qui implique
\begin{eqnarray*}
S &=& Z_v+H-H_d-Z_b.
\end{eqnarray*}
On consid�re deux cas en fonction du signe de $S$:
\begin{itemize}
\item $S<0$ : domaine de s�ret�,
\item $S>0$ : d�faillance (crue).
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Partie th�orique}

\begin{enumerate}
\item Montrer que, sous les hypoth�ses pr�c�dentes, on a :
$$
\alpha \approx \frac{Z_m-Z_v}{L}.
$$

\item Montrer que, sous les hypoth�ses pr�c�dentes, on a :
\begin{eqnarray*}
H \approx \left(\frac{Q}{K_s B \sqrt{\alpha}}\right)^{3/5}.
\end{eqnarray*}

\item Pourquoi la loi de Gumbel est-elle adapt�e pour 
mod�liser la variabilit� de $Q$ ?
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{D�finition du probl�me probabiliste}

Dans cette section, on consid�re des donn�es fictives, 
mais r�alistes, dont les ordres de grandeur pourraient correspondre 
� un fleuve fran�ais.

On suppose que les param�tres suivants sont donn�s:
\begin{itemize}
\item hauteur de la digue : $H_d=3$ (m),
\item c�te de la berge : $Z_b=55.5$ (m),
\item longueur du tron�on de rivi�re : $L=5000$ (m),
\item largeur de la rivi�re : $B=300$ (m).
\end{itemize}

On consid�re le mod�le 
\begin{eqnarray*}
S = g(Q,K_s,Z_v,Z_m),
\end{eqnarray*}
o� $Q$, $K_s$, $Z_v$ et $Z_m$ sont des variables al�atoires 
ind�pendantes suivantes:
\begin{itemize}
\item $Q$, le d�bit moyen de la rivi�re ($m^3/s$), de loi Gumbel de param�tres 1013 (mode) 
et 558 (�chelle),
\item $K_s$, le coefficient de Manning-Strickler de la rivi�re ($m^{1/3}$/s), 
de loi Normale de param�tres 30 (moyenne) et 7.5 (�cart-type),
\item $Z_v$, la c�te du fond de la rivi�re en aval (m), 
de loi uniforme entre 49 et 51,
\item $Z_m$, la c�te du fond de la rivi�re en amont (m), 
de loi uniforme entre 54 et 56.
\end{itemize}

Les variables physiques $Q$ et $K_s$ sont physiquement 
positives, de telle sorte que les distributions doivent �tre tronqu�es.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Partie pratique}

\begin{enumerate}
\item Dessiner la densit� de probabilit� des quatres variables al�atoires 
$Q$, $K_s$, $Z_v$ et $Z_m$. 
Indication~: le d�bit $Q$ suit une loi de type max-Gumbel, tandis que 
la fonction \scifun{evpdf} utilise des r�alisations de type 
min-Gumbel. 
Pour obtenir la distribution max-Gumbel, on doit 
prendre l'oppos� de \scivar{x} et l'oppos� de \scivar{mu}.
\item G�n�rer un plan d'exp�riences de type Monte-Carlo, 
avec \scivar{N=100000} exp�riences. 
Indication~: le d�bit $Q$ suit une loi de type max-Gumbel, tandis que 
la fonction \scifun{evrnd} g�n�re des r�alisations d'une variable de type 
min-Gumbel. 
\item Tronquer les exp�riences correspondant � des r�alisations 
$K_s$ et $Q$ physiquement impossibles. 
Indication~: le nombre d'exp�riences final \scivar{N} est alors un peu inf�rieur � \scivar{100000}, 
mais l'estimation statistique reste valide, car c'est comme si l'on 
avait g�n�r� des r�alisations selon une distribution tronqu�e.
\item Dessiner les histogrammes empiriques des quatre variables  al�atoires 
$Q$, $K_s$, $Z_v$ et $Z_m$. 
\item Dessiner un scatter plot de la surverse $S$ en fonction 
des quatre entr�es al�atoires $Q$, $K_s$, $Z_v$ et $Z_m$.
\item Estimer $P(S>0)$.
\item Estimer un intervalle de confiance � 95\% pour $P(S>0)$.
\item Calculer les quantiles � $1-10^{-1}$ (crue d�cennale), 
$1-10^{-2}$ (crue centennale), $1-10^{-3}$ (crue mill�nale).
\item Calculer les m�mes quantiles avec la m�thode de Wilks et 
95\% de confiance.
\item La digue est-elle bien dimensionn�e pour une crue mill�nale ?
\end{enumerate}

Le script suivant vous est donn�, dans lequel vous devez compl�ter 
les parties marqu�es \scivar{TODO}.

\lstset{language=scilabscript}
\lstinputlisting{projetCrue-sujet.sce}