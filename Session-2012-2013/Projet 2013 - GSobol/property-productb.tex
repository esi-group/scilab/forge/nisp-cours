%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The product}

\begin{proposition}
(\emph{Product of $1+g_i$})
\label{prop-prodg}
For $i=1,2,...,p$, consider the function $y_i=g_i(x_i)$, 
where $x_i,y_i \in\RR$. 
Therefore, 
\begin{eqnarray}
\label{eq-prodg6}
\prod_{i=1}^p (1 + g_i(x_i) )
&=& 1+\sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} g_i(x_i).
\end{eqnarray}
\end{proposition}

\begin{example}
For $p=2$, we have
\begin{eqnarray}
\label{eq-prodg6-1}
\prod_{i=1}^2 (1 + y_i )
&=& (1 + y_1)(1 + y_2) \\
&=& 1 + y_1 + y_2 + y_1 y_2,
\end{eqnarray}
which is consistent with the equation \ref{eq-prodg6}.
\end{example}

\begin{example}
For $p=3$, we have
\begin{eqnarray}
\label{eq-prodg6-2}
\prod_{i=1}^3 (1 + y_i )
&=& (1 + y_1)(1 + y_2)(1 + y_3) \\
&=& \left(1 + y_1 + y_2 + y_1 y_2\right) (1 + y_3) \\
&=& 1 + y_1 + y_2 + y_1 y_2 + y_3 + y_1 y_3 + y_2 y_3 + y_1 y_2 y_3
\end{eqnarray}
which is, again, consistent with the equation \ref{eq-prodg6}.
\end{example}

\begin{proof}
Let us prove the equation \ref{eq-prodg6} by induction. 
The proof is obvious for $p=1$, and we have checked 
it for $p=2$ and $p=3$. 
Let us assume that the equation \ref{eq-prodg6} is true for 
$p$ and let us prove that it is true for $p+1$. 
We have
\begin{eqnarray}
\label{eq-prodg6-3}
\prod_{i=1}^{p+1} (1 + y_i )
&=& (1 + y_{p+1} ) \prod_{i=1}^p (1 + g_i(x_i) ) \\
&=& (1 + y_{p+1} ) \left( 1+\sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} y_i \right) \\
&=& 1+\sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} y_i 
  + y_{p+1} + y_{p+1} \sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} y_i 
\label{eq-prodg6-4}
\end{eqnarray}
We have to prove that the right hand side of the previous 
equation is the sum for the set $\{1,2,...,p+1\}$. 

In order to do this, we split the sum for $p+1$ into the subsets $\bu$ which 
contain $p+1$ and those which do not contain $p+1$. 
On the other hand, we have
\begin{eqnarray}
\sum_{\substack{\bu\subseteq \{1,2,...,p+1\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} y_i 
&=& \sum_{\substack{\bu\subseteq \{1,2,...,p+1\}\\ \bu\neq \emptyset\\ \bu\ni p+1}} \prod_{i\in\bu} y_i 
 +\sum_{\substack{\bu\subseteq \{1,2,...,p+1\}\\ \bu\neq \emptyset\\ \bu\not\ni p+1}} \prod_{i\in\bu} y_i.
\end{eqnarray}
If $p+1$ is not in the set $\bu$, then we can remove it from the $\{1,2,...,p+1\}$ and get
\begin{eqnarray}
\sum_{\substack{\bu\subseteq \{1,2,...,p+1\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} y_i 
&=& \sum_{\substack{\bu\subseteq \{1,2,...,p+1\}\\ \bu\neq \emptyset\\ \bu\ni p+1}} \prod_{i\in\bu} y_i 
 +\sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} y_i.
\end{eqnarray}

We now simplify the sum for $\bu$ containing $p+1$. 
Notice that, there are two different cases. 
\begin{itemize}
\item Either there is only one element in the subset $\bu$ (i.e. $|\bu|=1$), 
so that that this element must be $\bu=p+1$. In this case, the product is equal to $y_{p+1}$. 
\item Or there are more than one element in the subset $\bu$ (i.e. $|\bu|>1$), 
so that that the subset $\bu$ can be written $\bu=(\bv,p+1)$, 
where $\bv\subseteq\{1,2,...,p\}$. 
In this case, the product can be factored by $y_{p+1}$, since all the subsets 
contain this element. 
\end{itemize}
Therefore,
\begin{eqnarray}
\sum_{\substack{\bu\subseteq \{1,2,...,p+1\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} y_i 
&=& y_{p+1} + y_{p+1} \sum_{\substack{\bv\subseteq \{1,2,...,p\}\\ \bv\neq \emptyset}} \prod_{i\in\bv} y_i 
 +\sum_{\substack{\bu\subseteq \{1,2,...,p\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} y_i.
\label{eq-prodg6-5}
\end{eqnarray}
We plug the equation \ref{eq-prodg6-5} into \ref{eq-prodg6-4}, and 
get
\begin{eqnarray}
\label{eq-prodg6-6}
\prod_{i=1}^{p+1} (1 + y_i )
&=& 1+\sum_{\substack{\bu\subseteq \{1,2,...,p+1\}\\ \bu\neq \emptyset}} \prod_{i\in\bu} y_i,
\end{eqnarray}
which concludes the proof.
\end{proof}
