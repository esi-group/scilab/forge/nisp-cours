// Copyright (C) 2013 - Michael Baudin
//
// Log-Uniform distribution
// Reference
// TODO

// If U is uniform in [a,b], then log(U) is LogUniform in [exp(a),exp(b)].
//
function y=logupdf(x,a,b)
    // Log-Uniform PDF, in [a,b]
    y=1 ./(b-a)./x
endfunction

function r=logurnd(a,b,m,n)
    // Log-Uniform random numbers, in [a,b]
    u=distfun_unifrnd(a,b,m,n)
    r=exp(u)
endfunction

function p=logucdf(x,a,b)
    // Log-Uniform CDF, in [a,b]
    p=(log(x)-a)./(b-a)
endfunction

function [M,V]=logustat(a,b)
    // Log-Uniform statistics, in [a,b]
    M=(exp(b)-exp(a))./(b-a)
    V=0.5*(exp(b)^2-exp(a)^2)./(b-a)-M^2
endfunction

// Check PDF
a=2;
b=3.5;
N=10000;
x=linspace(exp(a),exp(b),1000);
y=logupdf(x,a,b);
r=logurnd(a,b,N,1);
scf();
histplot(20,r)
plot(x,y)
xtitle("Log-Uniform","X","");
legend(["Data","PDF"]);
//
// Check CDF
a=2;
b=3.5;
N=10000;
x=linspace(exp(a),exp(b),1000);
y=logucdf(x,a,b);
r=logurnd(a,b,N,1);
r=gsort(r,"g","i");
scf();
plot(r,(1:N)./N,"r-");
plot(x,y,"b-")
xtitle("Log-Uniform","X","");
legend(["Data","CDF"]);
//
// Check statistics
a=2;
b=3.5;
N=100000;
[M,V]=logustat(a,b);
x=logurnd(a,b,N,1);
mprintf("M=%f, mean(X)=%f\n",M,mean(x));
mprintf("V=%f, variance(X)=%f\n",V,variance(x));


