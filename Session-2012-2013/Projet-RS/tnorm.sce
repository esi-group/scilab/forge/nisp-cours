// Copyright (C) 2013 - Michael Baudin
//
// Truncated normal distribution
// Reference
// http://en.wikipedia.org/wiki/Truncated_normal_distribution
//
function y=tnormpdf(x,mu,sigma,a,b)
    // Truncated normal PDF, 
    // in [a,b]
    xi=(x-mu)/sigma
    al=(a-mu)/sigma
    be=(b-mu)/sigma
    yxi=distfun_normpdf(xi,0,1)
    pal=distfun_normcdf(al,0,1)
    pbe=distfun_normcdf(be,0,1)
    z=pbe-pal    
    y=yxi/sigma/z
endfunction

if (%f) then
    a=-10;
    b=10;
    x=linspace(a,b,1000);
    y1=tnormpdf(x,-8,2,a,b);
    y2=tnormpdf(x,0,2,a,b);
    y3=tnormpdf(x,9,10,a,b);
    y4=tnormpdf(x,0,10,a,b);
    scf();
    plot(x,y1,"k-")
    plot(x,y2,"b-")
    plot(x,y3,"r-")
    plot(x,y4,"g-")
    xtitle("Truncated normal [-10,10]","X","PDF");
    legend(["$\mu=-8,\sigma=2$","$\mu=0,\sigma=2$","$\mu=9,\sigma=10$","$\mu=0,\sigma=10$"])
end

function p=tnormcdf(x,mu,sigma,a,b)
    // Truncated normal CDF, 
    // in [a,b]
    // TODO : add lowertail
    xi=(x-mu)/sigma
    al=(a-mu)/sigma
    be=(b-mu)/sigma
    pxi=distfun_normcdf(xi,0,1)
    pal=distfun_normcdf(al,0,1)
    pbe=distfun_normcdf(be,0,1)
    z=pbe-pal    
    p=(pxi-pal)/z
endfunction
if (%f) then
    a=-10;
    b=10;
    x=linspace(a,b,1000);
    y1=tnormcdf(x,-8,2,a,b);
    y2=tnormcdf(x,0,2,a,b);
    y3=tnormcdf(x,9,10,a,b);
    y4=tnormcdf(x,0,10,a,b);
    scf();
    plot(x,y1,"k-")
    plot(x,y2,"b-")
    plot(x,y3,"r-")
    plot(x,y4,"g-")
    xtitle("Truncated normal [-10,10]","X","CDF");
    legend(["$\mu=-8,\sigma=2$","$\mu=0,\sigma=2$","$\mu=9,\sigma=10$","$\mu=0,\sigma=10$"])
end

function x=tnorminv(p,mu,sigma,a,b)
    // Truncated normal Inverse CDF, 
    // in [a,b]
    // TODO : add lowertail
    al=(a-mu)/sigma
    be=(b-mu)/sigma
    pal=distfun_normcdf(al,0,1)
    pbe=distfun_normcdf(be,0,1)
    z=pbe-pal
    xi=distfun_norminv(pal+z*p,0,1)
    x=mu+sigma*xi
endfunction

if (%f) then
    a=-10;
    b=10;
    //
    mu=-8;
    sigma=2;
    x=linspace(a,b,1000);
    p1=tnormcdf(x,mu,sigma,a,b);
    x1=tnorminv(p1,mu,sigma,a,b);
    d1=assert_computedigits(x,x1);
    //
    mu=0;
    sigma=2;
    x=linspace(a,b,1000);
    p2=tnormcdf(x,mu,sigma,a,b);
    x2=tnorminv(p2,mu,sigma,a,b);
    d2=assert_computedigits(x,x2);
    //
    mu=9;
    sigma=10;
    x=linspace(a,b,1000);
    p3=tnormcdf(x,mu,sigma,a,b);
    x3=tnorminv(p3,mu,sigma,a,b);
    d3=assert_computedigits(x,x3);
    //
    mu=0;
    sigma=10;
    x=linspace(a,b,1000);
    p4=tnormcdf(x,mu,sigma,a,b);
    x4=tnorminv(p4,mu,sigma,a,b);
    d4=assert_computedigits(x,x4);
    //
    scf();
    subplot(2,2,1)
    plot(x,d1)
    xtitle("$\textrm{Inverse Truncated normal [-10,10], }\mu=-8,\sigma=2$","X","Digits");
    subplot(2,2,2)
    plot(x,d2)
    xtitle("$\textrm{Inverse Truncated normal [-10,10], }\mu=0,\sigma=2$","X","Digits");
    subplot(2,2,3)
    plot(x,d3)
    xtitle("$\textrm{Inverse Truncated normal [-10,10], }\mu=9,\sigma=10$","X","Digits");
    subplot(2,2,4)
    plot(x,d4)
    xtitle("$\textrm{Inverse Truncated normal [-10,10], }\mu=0,\sigma=10$","X","Digits");
end

function x=tnormrnd(mu,sigma,a,b,m,n)
    // Truncated normal random numbers, 
    // in [a,b]
    u=distfun_unifrnd(0,1,m,n)
    x=tnorminv(u,mu,sigma,a,b)
endfunction

if (%f) then
    a=-10;
    b=10;
    N=1000;
    x=linspace(a,b,1000);
    y1=tnormpdf(x,-8,2,a,b);
    x1=tnormrnd(-8,2,a,b,N,1);
    y2=tnormpdf(x,0,2,a,b);
    x2=tnormrnd(0,2,a,b,N,1);
    y3=tnormpdf(x,9,10,a,b);
    x3=tnormrnd(9,10,a,b,N,1);
    y4=tnormpdf(x,0,10,a,b);
    x4=tnormrnd(0,10,a,b,N,1);
    scf();
    subplot(2,2,1)
    histplot(20,x1);
    plot(x,y1);
    xtitle("$\textrm{Truncated normal [-10,10], }\mu=-8,\sigma=2$","X","Frequency");
    legend(["Data","PDF"])
    //
    subplot(2,2,2)
    histplot(20,x2);
    plot(x,y2);
    xtitle("$\textrm{Truncated normal [-10,10], }\mu=0,\sigma=2$","X","Frequency");
    legend(["Data","PDF"])
    //
    subplot(2,2,3)
    histplot(20,x3);
    plot(x,y3);
    xtitle("$\textrm{Truncated normal [-10,10], }\mu=9,\sigma=10$","X","Frequency");
    legend(["Data","PDF"])
    //
    subplot(2,2,4)
    histplot(20,x4);
    plot(x,y4);
    xtitle("$\textrm{Truncated normal [-10,10], }\mu=0,\sigma=10$","X","Frequency");
    legend(["Data","PDF"])
end

function [M,V]=tnormstat(mu,sigma,a,b)
    // Truncated normal statistics, 
    // in [a,b]
    al=(a-mu)/sigma
    be=(b-mu)/sigma
    yal=distfun_normpdf(al,0,1)
    ybe=distfun_normpdf(be,0,1)
    pal=distfun_normcdf(al,0,1)
    pbe=distfun_normcdf(be,0,1)
    z=pbe-pal
    M=mu+(yal-ybe)*sigma/z
    s=(al*yal-be*ybe)/z
    t=(yal-ybe)/z
    V=sigma^2*(1+s-t^2)
endfunction

if (%f) then
    a=-10;
    b=10;
    N=1000;
    //
    mu=-8;
    sigma=2;
    mprintf("mu=%f, sigma=%f\n",mu,sigma);
    [M,V]=tnormstat(mu,sigma,a,b);
    x=tnormrnd(mu,sigma,a,b,N,1);
    mprintf("M=%f, mean(X)=%f\n",M,mean(x));
    mprintf("V=%f, variance(X)=%f\n",V,variance(x));
    //
    mu=0;
    sigma=2;
    mprintf("mu=%f, sigma=%f\n",mu,sigma);
    [M,V]=tnormstat(mu,sigma,a,b);
    x=tnormrnd(mu,sigma,a,b,N,1);
    mprintf("M=%f, mean(X)=%f\n",M,mean(x));
    mprintf("V=%f, variance(X)=%f\n",V,variance(x));
    //
    mu=9;
    sigma=10;
    mprintf("mu=%f, sigma=%f\n",mu,sigma);
    [M,V]=tnormstat(mu,sigma,a,b);
    x=tnormrnd(mu,sigma,a,b,N,1);
    mprintf("M=%f, mean(X)=%f\n",M,mean(x));
    mprintf("V=%f, variance(X)=%f\n",V,variance(x));
    //
    mu=0;
    sigma=10;
    mprintf("mu=%f, sigma=%f\n",mu,sigma);
    [M,V]=tnormstat(mu,sigma,a,b);
    x=tnormrnd(mu,sigma,a,b,N,1);
    mprintf("M=%f, mean(X)=%f\n",M,mean(x));
    mprintf("V=%f, variance(X)=%f\n",V,variance(x));
end
