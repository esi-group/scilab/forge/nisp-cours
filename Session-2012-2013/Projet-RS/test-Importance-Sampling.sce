// Copyright (C) 2013 - Michael Baudin
// http://en.wikipedia.org/wiki/Importance_sampling
// Requires : distfun

mu=0;
sigma=1;
threshold=3;
// Je veux estimer P(X>threshold)
// Exact
mprintf("\nExact Computation\n")
[ignored,PfExact]=cdfnor("PQ",threshold,mu,sigma);
mprintf("PfExact = %e\n" , PfExact);
/////////////////////////////////////////////
//
// Par Monte-Carlo
//
mprintf("\nMonte-Carlo\n")
NbSim=10^6;
mprintf("Number of simulations=%d\n" , NbSim);
X=grand(NbSim,1,"nor",mu,sigma);
// Plot the histogram
scf();
histplot(20,X)
plot([threshold,threshold],[0.,0.5],"r-")
legend(["X","Threshold"]);
//
failed = find(X>threshold);
nfail = size(failed,"*");
Pf = nfail/NbSim;
mprintf("Number of failures=%d (%.1f %%)\n" , nfail,100*nfail/NbSim);
mprintf("Pf = %e (exact=%e)\n" , Pf,PfExact);

// 9. Compute a confidence interval of Pf
level=1.-0.95;
q = level/2.;
p = 1.-q;
f = cdfnor("X",0.,1.,p,q);
low = Pf - f * sqrt(Pf*(1.-Pf)/NbSim);
up = Pf + f * sqrt(Pf*(1.-Pf)/NbSim);
mprintf("95%% Conf. Int.:[%e,%e]\n" , low,up);
///////////////////////////////////////////////////
// Plot the conditionnal distribution X|X>t
scf();
Y=X(failed);
histplot(20,Y)
xtitle("Distribution X|X>t","X","")


///////////////////////////////////////////////////
//
exec("tnorm.sce");

scf();
Y=X(failed);
histplot(20,Y)
x=linspace(threshold,threshold+2);
y=tnormpdf(x,mu,sigma,threshold,%inf);
plot(x,y)
xtitle("Distribution X|X>t","X","")
legend(["Data","Truncated Normal"]);

///////////////////////////////////////////////////
//
// Importance sampling with optimum importance density
//
mprintf("\nImportance Sampling\n")
NbSim=10;
muIS=mu;
sigmaIS=sigmaIS;
mprintf("Number of simulations=%d\n" , NbSim);
X=tnormrnd(muIS,sigmaIS,threshold,%inf,NbSim,1);
g=zeros(X);
failed = find(X>threshold);
nfail = size(failed,"*");
g(failed)=1;
f=distfun_normpdf(X,mu,sigma);
h=tnormpdf(X,muIS,sigmaIS,threshold,%inf);
Y=g.*f./h;
Pf=mean(Y);
mprintf("Number of failures=%d (%.1f %%)\n" , nfail,100*nfail/NbSim);
mprintf("Pf = %e (exact=%e)\n" , Pf,PfExact);
mprintf("Variance(Y) = %e\n" , variance(Y)/NbSim);

///////////////////////////////////////////////////
//
// Importance sampling with shifted importance density
//

mprintf("\nImportance Sampling\n")
NbSim=1000;
muIS=3;
sigmaIS=1;
mprintf("muIS=%f, sigmaIS=%f\n" , muIS, sigmaIS);
mprintf("Number of simulations=%d\n" , NbSim);
X=distfun_normrnd(muIS,sigmaIS,NbSim,1);
g=zeros(X);
failed = find(X>threshold);
nfail = size(failed,"*");
g(failed)=1;
f=distfun_normpdf(X,mu,sigma);
h=distfun_normpdf(X,muIS,sigmaIS);
Y=g.*f./h;
Pf=mean(Y);
mprintf("Number of failures=%d (%.1f %%)\n" , nfail,100*nfail/NbSim);
mprintf("Pf = %e (exact=%e)\n" , Pf,PfExact);
mprintf("Variance(Y) = %e\n" , variance(Y)/NbSim);

