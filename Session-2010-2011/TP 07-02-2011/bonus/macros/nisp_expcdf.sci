// Copyright (C) 2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html


function p = nisp_expcdf ( x , lambda )
  // Computes the Exponential CDF.
  //
  // Calling Sequence
  //   p = nisp_expcdf ( x , lambda )
  //
  // Parameters
  // x: a matrix of doubles
  // lambda : a matrix of doubles
  // p: a matrix of doubles, the probability
  //
  // Description
  //   This function computes the Exponential CDF.
  // This function is vectorized but all the input arguments must have the same size.
  // 
  // TODO : improve implementation (check arguments, check inf, nan, etc...)
  //
  // Examples
  // // http://en.wikipedia.org/wiki/Exponential_distribution
  // scf();
  // x = linspace(0,5,1000);
  // p = nisp_expcdf ( x , 0.5 );
  // plot(x,p, "r-" );
  // p = nisp_expcdf ( x , 1 );
  // plot(x,p, "m-" );
  // p = nisp_expcdf ( x , 1.5 );
  // plot(x,p, "c-" );
  // xtitle("Exponential Cumulated Distribution Function","X","P(X)");
  // legend(["lambda=0.5","lambda=","lambda=1.5"]);
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin
  //
  // Bibliography
  // Wikipedia, Exponential distribution function, http://en.wikipedia.org/wiki/Exponential_distribution

  q = exp(-lambda.*x)
  p = 1-q
endfunction

