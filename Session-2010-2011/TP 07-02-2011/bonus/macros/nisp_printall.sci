// Copyright (C) 2008-2011 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function nisp_printall ( )
  // Prints all current objects.
  //
  // Calling Sequence
  //   nisp_printall ( )
  //
  // Description
  //   This function allows to print a summary of all objects currently in use.
  //
  // Examples
  // nisp_printall ( )
  //
  // // Create three variables, put them into a set.
  // rvu1 = randvar_new("Uniforme",-%pi,%pi);
  // rvu2 = randvar_new("Uniforme",-%pi,%pi);
  // rvu3 = randvar_new("Uniforme",-%pi,%pi);
  // srvu = setrandvar_new();
  // setrandvar_addrandvar ( srvu, rvu1);
  // setrandvar_addrandvar ( srvu, rvu2);
  // setrandvar_addrandvar ( srvu, rvu3);
  // nisp_printall ( )
  // // Clean-up
  // setrandvar_destroy(srvu);
  // randvar_destroy(rvu1);
  // randvar_destroy(rvu2);
  // randvar_destroy(rvu3);
  // // There must be no token anymore.
  // nisp_printall ( )
  //
  // Authors
  // Copyright (C) 2008-2011 - INRIA - Michael Baudin

  mprintf ( "Class randvar\n")
  mprintf ( "  Number of objects : %d\n", randvar_size())
  mprintf ( "  Tokens : %s\n", strcat ( string ( randvar_tokens() ) ," " ) )
  mprintf ( "Class setrandvar\n")
  mprintf ( "  Number of objects : %d\n", setrandvar_size())
  mprintf ( "  Tokens : %s\n", strcat ( string ( setrandvar_tokens() ) ," " ) )
  mprintf ( "Class polychaos\n")
  mprintf ( "  Number of objects : %d\n", polychaos_size())
  mprintf ( "  Tokens : %s\n", strcat ( string ( polychaos_tokens() ) ," " ) )
endfunction

