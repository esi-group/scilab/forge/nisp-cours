// Copyright (C) 2009 - CEA - Jean-Marc Martinez
// Copyright (C) 2009-2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

function r = nisp_corrcoef ( x , y )
    // Returns the linear correlation coefficient of x and y.
  //
  // Calling Sequence
  //   r = nisp_corrcoef ( x , y )
  //
  // Parameters
  // x: a n-by-1 matrix of doubles
  // y: a n-by-1 matrix of doubles
  // r: a 1-by-1 matrix of doubles, the linear correlation coefficient.
  //
  // Description
  //   Returns the linear correlation coefficient of x and y.
  //
  // Examples
  // // "Introduction to probability and statistics for 
  // // engineers and scientists."
  // // Sheldon Ross
  // // Chapter 2 Descriptive statistics
  // // Example 2.6a
  // x = [24.2;22.7;30.5;28.6;25.5;32;28.6;26.5;25.3;26;24.4;24.8;20.6;..
  // 25.1;21.4;23.7;23.9;25.2;27.4;28.3;28.8;26.6];
  // y = [25;31;36;33;19;24;27;25;16;14;22;23;20;25;25;23;27;30;33;32;35;24];
  // expected = 0.4189
  // r = nisp_corrcoef ( x , y )
  //
  // Authors
  // Copyright (C) 2011 - INRIA - Michael Baudin
  //
  // Bibliography
  // "Introduction to probability and statistics for engineers and scientists.", Sheldon Ross

    x = x(:)
    y = y(:)
    x = x - mean(x)
    y = y - mean(y)
    sx = sqrt(sum(x.^2))
    sy = sqrt(sum(y.^2))
    r = x'*x / sx / sy
endfunction

