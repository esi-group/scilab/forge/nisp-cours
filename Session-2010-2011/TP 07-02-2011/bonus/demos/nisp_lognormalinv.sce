//
// This help file was automatically generated from nisp_lognormalinv.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_lognormalinv.sci
//

//
// Check the inverse lognormale
mu = 1;
sigma=10;
x=2;
p = nisp_lognormalcdf ( x , mu , sigma ); // 0.4877603
x = nisp_lognormalinv ( p , mu , sigma ) // Must be 2
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_lognormalinv.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
