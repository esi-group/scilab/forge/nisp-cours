//
// This help file was automatically generated from nisp_printall.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_printall.sci
//

nisp_printall ( )
halt()   // Press return to continue
 
// Create three variables, put them into a set.
rvu1 = randvar_new("Uniforme",-%pi,%pi);
rvu2 = randvar_new("Uniforme",-%pi,%pi);
rvu3 = randvar_new("Uniforme",-%pi,%pi);
srvu = setrandvar_new();
setrandvar_addrandvar ( srvu, rvu1);
setrandvar_addrandvar ( srvu, rvu2);
setrandvar_addrandvar ( srvu, rvu3);
nisp_printall ( )
// Clean-up
setrandvar_destroy(srvu);
randvar_destroy(rvu1);
randvar_destroy(rvu2);
randvar_destroy(rvu3);
// There must be no token anymore.
nisp_printall ( )
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_printall.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
