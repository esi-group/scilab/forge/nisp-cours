//
// This help file was automatically generated from nisp_exppdf.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of nisp_exppdf.sci
//

// http://en.wikipedia.org/wiki/Exponential_distribution
scf();
x = linspace(0,5,1000);
p = nisp_exppdf ( x , 0.5 );
plot(x,p, "r-" );
p = nisp_exppdf ( x , 1 );
plot(x,p, "m-" );
p = nisp_exppdf ( x , 1.5 );
plot(x,p, "c-" );
xtitle("Exponential Probability Distribution Function","X","P(X)");
legend(["lambda=0.5","lambda=","lambda=1.5"]);
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "nisp_exppdf.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
